package model

import (
	"fmt"
	"rpc_server/contrib/helper"

	"github.com/meilisearch/meilisearch-go"
	rysrv "github.com/ryrpc/server"
	"github.com/valyala/fasthttp"
)

type messageData struct {
	T int64        `json:"t" cbor:"t"`
	S int          `json:"s" cbor:"s"`
	D []TblMessage `json:"d" cbor:"d"`
}

type messageBulkParam struct {
	Title    string   `cbor:"title"`
	Content  string   `cbor:"content"`
	SendName string   `cbor:"sendName"`
	IsTop    int      `cbor:"isTop"`
	IsVip    int      `cbor:"isVip"`
	Ty       int      `cbor:"ty"`
	Names    []string `cbor:"names"`
}

type messageListParam struct {
	Name string `cbor:"name"`
	Ty   int    `cbor:"ty"`
	Read int    `cbor:"read"`
	Page int    `cbor:"page"`
	Size int    `cbor:"size"`
}

func MessageUnreadTotal(fctx *fasthttp.RequestCtx) {

	params := string(fctx.PostArgs().Peek("params"))

	cond := &meilisearch.SearchRequest{
		Limit: 2,
	}

	filter := fmt.Sprintf("username = %s AND is_read = 1", params)

	index := meili(fctx).Index("message")

	cond.Filter = filter
	searchRes, err := index.Search("", cond)
	if err != nil {
		rysrv.SetError(fctx, err)
		return
	}

	rysrv.SetResult(fctx, searchRes.EstimatedTotalHits)
}

func MessageUpdate(fctx *fasthttp.RequestCtx) {

	var doc TblMessage

	params := string(fctx.PostArgs().Peek("params"))

	index := meili(fctx).Index("message")
	err := index.GetDocument(params, nil, &doc)
	if err != nil {
		rysrv.SetError(fctx, err)
		return
	}
	if doc.IsRead == 2 {
		rysrv.SetResult(fctx, true)
		return
	}

	doc.IsRead = 2

	b, err := helper.JsonMarshal(doc)
	if err != nil {
		fmt.Println("messageSend JsonMarshal err = ", err.Error())
		rysrv.SetError(fctx, err)
		return
	}

	_, err = index.AddDocuments(b, "id")
	if err != nil {
		fmt.Println("messageSend AddDocuments err = ", err.Error())
		rysrv.SetError(fctx, err)
		return
	}
	rysrv.SetResult(fctx, true)
}

// 群发
func MessageBulk(fctx *fasthttp.RequestCtx) {

	var (
		record []TblMessage
		params messageBulkParam
	)

	err := rysrv.Unmarshal(fctx, &params)
	if err != nil {
		rysrv.SetError(fctx, err)
		return
	}

	for _, v := range params.Names {
		recs := TblMessage{
			Id:       helper.GenId(),
			Username: v,
			Title:    params.Title,
			Content:  params.Content,
			IsTop:    params.IsTop,
			IsVip:    params.IsVip,
			Ty:       params.Ty,
			IsRead:   1,
			SendName: params.SendName,
			SendAt:   fctx.Time().UnixMilli(),
		}

		record = append(record, recs)
	}

	b, err := helper.JsonMarshal(record)
	if err != nil {
		fmt.Println("messageSend JsonMarshal err = ", err.Error())
		rysrv.SetError(fctx, err)
		return
	}

	index := meili(fctx).Index("message")
	_, err = index.AddDocuments(b, "id")
	if err != nil {
		fmt.Println("messageSend AddDocuments err = ", err.Error())
		rysrv.SetError(fctx, err)
		return
	}
	rysrv.SetResult(fctx, true)
}

func MessageDeleteById(fctx *fasthttp.RequestCtx) {

	var (
		params []string
	)

	err := rysrv.Unmarshal(fctx, &params)
	if err != nil {
		rysrv.SetError(fctx, err)
		return
	}

	index := meili(fctx).Index("message")
	_, err = index.DeleteDocuments(params)
	if err != nil {
		rysrv.SetError(fctx, err)
		return
	}
	rysrv.SetResult(fctx, true)
}

func MessageDeleteByFilter(fctx *fasthttp.RequestCtx) {

	params := string(fctx.PostArgs().Peek("params"))

	//fmt.Println("MessageDeleteByFilter params = ", params)
	index := meili(fctx).Index("message")
	_, err := index.DeleteDocumentsByFilter(params)
	if err != nil {
		rysrv.SetError(fctx, err)
		return
	}
	rysrv.SetResult(fctx, true)
}

func MessageList(fctx *fasthttp.RequestCtx) {

	var (
		data   messageData
		params messageListParam
	)

	err := rysrv.Unmarshal(fctx, &params)
	if err != nil {
		rysrv.SetError(fctx, err)
		return
	}

	offset_t := params.Size * (params.Page - 1)

	cond := &meilisearch.SearchRequest{
		Limit:  int64(params.Size),
		Offset: int64(offset_t),
	}

	filter := fmt.Sprintf("username = %s", params.Name)
	if params.Ty > 0 {
		filter += fmt.Sprintf(" AND ty = %d", params.Ty)
	}
	if params.Read > 0 {
		filter += fmt.Sprintf(" AND is_read = %d", params.Read)
	}
	index := meili(fctx).Index("message")

	cond.Filter = filter
	searchRes, err := index.Search("", cond)
	if err != nil {
		rysrv.SetError(fctx, err)
		return
	}
	ll := len(searchRes.Hits)
	if ll == 0 {
		data.S = 0
		data.T = 0
		rysrv.SetResult(fctx, data)
		return
	}

	data.D = make([]TblMessage, ll)
	data.T = searchRes.EstimatedTotalHits
	data.S = params.Size

	for i, v := range searchRes.Hits {
		val := v.(map[string]interface{})

		data.D[i].Id = val["id"].(string)
		data.D[i].Username = val["username"].(string)
		data.D[i].Title = val["title"].(string)
		data.D[i].Content = val["content"].(string)
		data.D[i].SendName = val["send_name"].(string)
		data.D[i].IsTop = int(val["is_top"].(float64))
		data.D[i].IsVip = int(val["is_vip"].(float64))
		data.D[i].Ty = int(val["ty"].(float64))
		data.D[i].IsRead = int(val["is_read"].(float64))
		data.D[i].SendAt = int64(val["send_at"].(float64))

	}

	rysrv.SetResult(fctx, data)
}

func MessageInit(fctx *fasthttp.RequestCtx) {

	filterable := []string{
		"username",
		"is_read",
		"is_vip",
		"ty",
		"send_at",
	}

	sortable := []string{
		"send_at",
	}
	searchable := []string{
		"content",
	}

	meili(fctx).DeleteIndex("message")
	index := meili(fctx).Index("message")
	index.UpdateFilterableAttributes(&filterable)
	index.UpdateSortableAttributes(&sortable)
	index.UpdateSearchableAttributes(&searchable)

	rysrv.SetResult(fctx, true)
}
