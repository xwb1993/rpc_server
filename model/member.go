package model

import (
	rysrv "github.com/ryrpc/server"
	"github.com/valyala/fasthttp"
)

func MemberTest(fctx *fasthttp.RequestCtx) {

	params := string(fctx.PostArgs().Peek("params"))

	rysrv.SetResult(fctx, params+" success")
}
