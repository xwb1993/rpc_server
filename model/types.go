package model

type TblAdminGroup struct {
	CreateAt   int64  `db:"create_at" json:"create_at" cbor:"create_at"`    //创建时间
	Gid        string `db:"gid" json:"gid" cbor:"gid"`                      //
	Gname      string `db:"gname" json:"gname" cbor:"gname"`                //组名
	Lft        int64  `db:"lft" json:"lft" cbor:"lft"`                      //节点左值
	Lvl        int64  `db:"lvl" json:"lvl" cbor:"lvl"`                      //
	Noted      string `db:"noted" json:"noted" cbor:"noted"`                //备注信息
	Permission string `db:"permission" json:"permission" cbor:"permission"` //权限模块ID
	Rgt        int64  `db:"rgt" json:"rgt" cbor:"rgt"`                      //节点右值
	Pid        string `db:"pid" json:"pid" cbor:"pid"`                      //父节点
	State      int    `db:"state" json:"state" cbor:"state"`                //0:关闭1:开启
}

type TblAdminGroupTree struct {
	Ancestor   string `db:"ancestor" json:"ancestor" cbor:"ancestor"`
	Descendant string `db:"descendant" json:"descendant" cbor:"descendant"`
	Lvl        int64  `db:"lvl" json:"lvl" cbor:"lvl"`
}

type TblAdminPriv struct {
	Id        string `db:"id" json:"id" cbor:"create_at"`
	Name      string `db:"name" json:"name" cbor:"name"`
	Module    string `db:"module" json:"module" cbor:"module"`
	Sortlevel string `db:"sortlevel" json:"sortlevel" cbor:"sortlevel"`
	Pid       string `db:"pid" json:"pid" cbor:"pid"`       //父节点
	State     int    `db:"state" json:"state" cbor:"state"` //0:关闭1:开启
}

type TblAdmins struct {
	ID            string `cbor:"id" db:"id" json:"id"`                                        // 主键ID
	GroupID       string `cbor:"group_id" db:"group_id" json:"group_id"`                      // 用户组ID
	LastLoginIP   string `cbor:"last_login_ip" db:"last_login_ip"`                            // 最后登录IP
	LastLoginTime uint32 `cbor:"last_login_time" db:"last_login_time" json:"last_login_time"` // 最后登录时间
	Pwd           string `cbor:"password" db:"password" json:"password"`                      // 密码
	State         int    `cbor:"state" db:"state" json:"state"`                               // 状态
	Seamo         string `cbor:"seamo" db:"seamo" json:"seamo"`
	Name          string `cbor:"name" db:"name" json:"name"`                         // 用户名
	CreateAt      uint32 `cbor:"create_at" db:"create_at" json:"create_at"`          // 创建时间
	CreatedUid    string `cbor:"created_uid" db:"created_uid" json:"created_uid"`    //创建人uid
	CreatedName   string `cbor:"created_name" db:"created_name" json:"created_name"` //创建人名
	UpdatedAt     uint32 `cbor:"updated_at" db:"updated_at" json:"updated_at"`       // 修改时间
	UpdatedUid    string `cbor:"updated_uid" db:"updated_uid" json:"updated_uid"`    //修改人uid
	UpdatedName   string `cbor:"updated_name" db:"updated_name" json:"updated_name"` //修改人名
}

type TblAppUpgrade struct {
	ID          string `db:"id" json:"id"`
	Platform    string `db:"platform" json:"platform"`
	Version     string `db:"version" json:"version"`
	IsForce     uint8  `db:"is_force" json:"is_force"`
	Content     string `db:"content" json:"content"`
	URL         string `db:"url" json:"url"`
	UpdatedAt   uint32 `db:"updated_at" json:"updated_at"`
	UpdatedUid  string `db:"updated_uid" json:"updated_uid"`
	UpdatedName string `db:"updated_name" json:"updated_name"`
}

type TblBalanceTransaction struct {
	AfterAmount  string `db:"after_amount" json:"after_amount" cbor:"after_amount"`    //账变后的金额
	Amount       string `db:"amount" json:"amount" cbor:"amount"`                      //用户填写的转换金额
	BeforeAmount string `db:"before_amount" json:"before_amount" cbor:"before_amount"` //账变前的金额
	BillNo       string `db:"bill_no" json:"bill_no" cbor:"bill_no"`                   //转账|充值|提现ID
	CashType     int    `db:"cash_type" json:"cash_type" cbor:"cash_type"`             //0:转入1:转出2:转入失败补回3:转出失败扣除4:存款5:提现
	CreatedAt    int64  `db:"created_at" json:"created_at" cbor:"created_at"`          //
	ID           string `db:"id" json:"id" cbor:"id"`                                  //
	UID          string `db:"uid" json:"uid" cbor:"uid"`                               //用户ID
	Username     string `db:"username" json:"username" cbor:"username"`                //用户名
	Remark       string `db:"remark" json:"remark" cbor:"remark"`                      //备注
	OperationNo  string `db:"operation_no" json:"operation_no" cbor:"operation_no"`    //操作码
	PlatformID   string `db:"platform_id" json:"platform_id" cbor:"platform_id"`       //场馆id
	Tester       int    `db:"tester" json:"tester" cbor:"tester"`                      //1正式 2测试 3代理
}

type TblBankType struct {
	Id       int    `json:"id" db:"id" cbor:"id"`
	BankName string `json:"bankname" db:"bankname" cbor:"bankname"`
	BankCode string `json:"bankcode" db:"bankcode" cbor:"bankcode"`
	State    int    `json:"state" db:"state" cbor:"state"`
}

type TblBanner struct {
	Id          string `db:"id" json:"id"`
	Title       string `db:"title" json:"title"`               //标题
	RedirectUrl string `db:"redirect_url" json:"redirect_url"` //跳转地址,多个地址用逗号分隔
	Images      string `db:"images" json:"images"`             //图片路径
	Seq         int    `db:"seq" json:"seq"`                   //排序
	UrlType     int    `db:"url_type" json:"url_type"`         //链接类型 1站内 2站外
	UpdatedName string `db:"updated_name" json:"updated_name"` //更新人name
	UpdatedUid  string `db:"updated_uid" json:"updated_uid"`   //更新人id
	UpdatedAt   uint32 `db:"updated_at" json:"updated_at"`     //更新时间
	State       string `db:"state" json:"state"`               //1待发布 2开启 3停用
	CreatedAt   uint32 `db:"created_at" json:"created_at"`
}

type TblBonusConfig struct {
	Id                string  `db:"id" cbor:"id" json:"id"`
	Name              string  `db:"name" cbor:"name" json:"name"`
	Code              string  `db:"code" cbor:"code" json:"code"`
	RechargeAmountLv1 float64 `db:"recharge_amount_lv1" cbor:"recharge_amount_lv1" json:"recharge_amount_lv1"`
	RateAmountLv1     float64 `db:"rate_amount_lv1" cbor:"rate_amount_lv1" json:"rate_amount_lv1"`
	RechargeAmountLv2 float64 `db:"recharge_amount_lv2" cbor:"recharge_amount_lv2" json:"recharge_amount_lv2"`
	RateAmountLv2     float64 `db:"rate_amount_lv2" cbor:"rate_amount_lv2" json:"rate_amount_lv2"`
	RechargeAmountLv3 float64 `db:"recharge_amount_lv3" cbor:"recharge_amount_lv3" json:"recharge_amount_lv3"`
	RateAmountLv3     float64 `db:"rate_amount_lv3" cbor:"rate_amount_lv3" json:"rate_amount_lv3"`
	LvlOneRebate      float64 `db:"lvl_one_rebate" cbor:"lvl_one_rebate" json:"lvl_one_rebate"`
	LvlTwoRebate      float64 `db:"lvl_two_rebate" cbor:"lvl_two_rebate" json:"lvl_two_rebate"`
	LvlThreeRebate    float64 `db:"lvl_three_rebate" cbor:"lvl_three_rebate" json:"lvl_three_rebate"`
}

type TblBusinessInfo struct {
	Id           string `json:"id" db:"id"`
	AccountName  string `json:"account_name" db:"account_name"`
	LoginAccount string `json:"login_account" db:"login_account"`
	Password     string `json:"password" db:"password"`
	InviteUrl    string `json:"invite_url" db:"invite_url"`
	OperatorId   string `json:"operator_id" db:"operator_id"`
	CreatedAt    int64  `json:"created_at" db:"created_at"`
	UpdatedAt    int64  `json:"updated_at" db:"updated_at"`
	Type         string `json:"type" db:"type"`
	ParentId     string `json:"parent_id" db:"parent_id"`
	ParentName   string `json:"parent_name" db:"parent_name"`
}

type tblDeposit struct {
	Id           string  `json:"id" db:"id" cbor:"id"`
	Oid          string  `json:"oid" db:"oid" cbor:"oid"`
	Uid          string  `json:"uid" db:"uid" cbor:"uid"`
	ParentId     string  `json:"parent_id" db:"parent_id" cbor:"parent_id"`
	ParentName   string  `json:"parent_name" db:"parent_name" cbor:"parent_name"`
	Username     string  `json:"username" db:"username" cbor:"username"`
	Fid          string  `json:"fid" db:"fid" cbor:"fid"`
	Fname        string  `json:"fname" db:"fname" cbor:"fname"`
	Amount       string  `json:"amount" db:"amount" cbor:"amount"`
	State        int     `json:"state" db:"state" cbor:"state"`
	CreatedAt    int64   `json:"created_at" db:"created_at" cbor:"created_at"`
	CreatedUid   string  `json:"created_uid" db:"created_uid" cbor:"created_uid"`
	CreatedName  string  `json:"created_name" db:"created_name" cbor:"created_name"`
	ConfirmAt    int64   `json:"confirm_at" db:"confirm_at" cbor:"confirm_at"`
	ConfirmUid   string  `json:"confirm_uid" db:"confirm_uid" cbor:"confirm_uid"`
	ConfirmName  string  `json:"confirm_name" db:"confirm_name" cbor:"confirm_name"`
	ReviewRemark string  `json:"review_remark" db:"review_remark" cbor:"review_remark"`
	TopId        string  `json:"top_id" db:"top_id" cbor:"top_id"`
	TopName      string  `json:"top_name" db:"top_name" cbor:"top_name"`
	Level        int     `json:"level" db:"level" cbor:"level"`
	Discount     float64 `json:"discount" db:"discount" cbor:"discount"`
	Tester       int     `json:"tester" db:"tester" cbor:"tester"`
	SuccessTime  int     `json:"success_time" db:"success_time" cbor:"success_time"`
	UsdtRate     float64 `json:"usdt_rate" db:"usdt_rate" cbor:"usdt_rate"`
	UsdtCount    float64 `json:"usdt_count" db:"usdt_count" cbor:"usdt_count"`
	Prefix       string  `json:"prefix" db:"prefix" cbor:"prefix"`
}

type TblFlowConfig struct {
	Id           string  `json:"id"  cbor:"id" db:"id"`
	Name         string  `json:"name" cbor:"name" db:"name"`
	Ty           int     `json:"ty" cbor:"ty" db:"ty"`
	FlowMultiple float64 `json:"flow_multiple" cbor:"flow_multiple" db:"flow_multiple"`
}

type TblGameConfig struct {
	Id          string `db:"id" json:"id"`
	CfgType     string `db:"cfg_type"  json:"cfg_type"`
	CfgValue    string `db:"cfg_value"  json:"cfg_value"`
	Description string `db:"description" json:"description"`
	TypeId      string `db:"type_id"  json:"type_id"`
}

type TblGameLists struct {
	ID         string `json:"id" db:"id" cbor:"id"`
	PlatformID string `json:"platform_id" db:"platform_id" cbor:"platform_id"` //场馆ID
	Name       string `json:"name" db:"name" cbor:"name"`                      //游戏名称
	EnName     string `json:"en_name" db:"en_name" cbor:"en_name"`             //英文名称
	BrAlias    string `json:"br_alias" db:"br_alias" cbor:"br_alias"`          //巴西别名
	ClientType string `json:"client_type" db:"client_type" cbor:"client_type"` //0:all 1:web 2:h5 4:app 此处值为支持端的数值之和
	GameType   int    `json:"game_type" db:"game_type" cbor:"game_type"`       //游戏类型:1=真人,2=捕鱼,3=电子,4=体育
	GameID     string `json:"game_id" db:"game_id" cbor:"game_id"`             //游戏ID
	Img        string `json:"img" db:"img" cbor:"img"`                         //手机图片
	Online     int    `json:"online" db:"online" cbor:"online"`                //0 下线 1上线
	IsHot      int    `json:"is_hot" db:"is_hot" cbor:"is_hot"`                //0 正常 1热门
	IsFav      int    `json:"is_fav" db:"is_fav" cbor:"is_fav"`                //0 正常 1热门
	IsNew      int    `json:"is_new" db:"is_new" cbor:"is_new"`                //是否最新:0=否,1=是
	Sorting    int    `json:"sorting" db:"sorting" cbor:"sorting"`             //排序
	CreatedAt  int64  `json:"created_at" db:"created_at" cbor:"created_at"`    //添加时间
	TagId      string `db:"tag_id" json:"tag_id" cbor:"tag_id"`                //标签集合
}

type TblGameRecord struct {
	RowId          string  `db:"row_id" json:"row_id" cbor:"row_id"`
	BillNo         string  `db:"bill_no" json:"bill_no" cbor:"bill_no"`
	ApiType        string  `db:"api_type" json:"api_type" cbor:"api_type"`
	PlayerName     string  `db:"player_name" json:"player_name" cbor:"player_name"`
	Name           string  `db:"name" json:"name" cbor:"name"`
	Uid            string  `db:"uid" json:"-" cbor:"uid"`
	TopUid         string  `db:"top_uid" json:"-"`                                                 //总代uid
	TopName        string  `db:"top_name" json:"top_name"`                                         //总代代理
	ParentUid      string  `db:"parent_uid" json:"parent_uid" cbor:"parent_uid"`                   //上级uid
	ParentName     string  `db:"parent_name" json:"parent_name" cbor:"parent_name"`                //上级代理
	GrandID        string  `db:"grand_id" json:"grand_id"  cbor:"grand_id"`                        //祖父级代理id
	GrandName      string  `db:"grand_name" json:"grand_name" cbor:"grand_name"`                   //祖父级代理
	GreatGrandID   string  `db:"great_grand_id" json:"great_grand_id"  cbor:"great_grand_id"`      //曾祖父级代理id
	GreatGrandName string  `db:"great_grand_name" json:"great_grand_name" cbor:"great_grand_name"` //曾祖父级代理
	NetAmount      float64 `db:"net_amount" json:"net_amount" cbor:"net_amount"`
	BetTime        int64   `db:"bet_time" json:"bet_time" cbor:"bet_time"`
	SettleTime     int64   `db:"settle_time" json:"settle_time" cbor:"settle_time"`
	ApiBetTime     int64   `db:"api_bet_time" json:"api_bet_time" cbor:"api_bet_time"`
	ApiSettleTime  int64   `db:"api_settle_time" json:"api_settle_time" cbor:"api_settle_time"`
	StartTime      int64   `db:"start_time" json:"start_time" cbor:"start_time"`
	Resettle       uint8   `db:"resettle" json:"resettle" cbor:"resettle"`
	Presettle      uint8   `db:"presettle" json:"presettle" cbor:"presettle"`
	GameType       string  `db:"game_type" json:"game_type" cbor:"game_type"`
	GameCode       string  `db:"game_code" json:"game_code" cbor:"game_code"`
	BetAmount      float64 `db:"bet_amount" json:"bet_amount" cbor:"bet_amount"`
	ValidBetAmount float64 `db:"valid_bet_amount" json:"valid_bet_amount" cbor:"valid_bet_amount"`
	RebateAmount   float64 `db:"-" json:"rebate_amount" cbor:"rebate_amount"`
	Flag           int     `db:"flag" json:"flag" cbor:"flag"`
	PlayType       string  `db:"play_type" json:"play_type" cbor:"play_type"`
	Prefix         string  `db:"prefix" json:"prefix" cbor:"prefix"`
	Result         string  `db:"result" json:"result" cbor:"result"`
	CreatedAt      int64   `db:"created_at" json:"-" cbor:"created_at"`
	UpdatedAt      int64   `db:"updated_at" json:"-" cbor:"updated_at"`
	ApiName        string  `db:"api_name" json:"api_name" cbor:"api_name"`
	ApiBillNo      string  `db:"api_bill_no" json:"api_bill_no" cbor:"api_bill_no"`
	MainBillNo     string  `db:"main_bill_no" json:"main_bill_no" cbor:"main_bill_no"`
	GameName       string  `db:"game_name" json:"game_name" cbor:"game_name"`
	HandicapType   string  `db:"handicap_type" json:"handicap_type" cbor:"handicap_type"`
	Handicap       string  `db:"handicap" json:"handicap" cbor:"handicap"`
	Odds           float64 `db:"odds" json:"odds" cbor:"odds"`
	Tester         int     `db:"tester" json:"tester" cbor:"tester"`
}

type TblGameTagIdx struct {
	Id     int `json:"id" db:"id"`
	TagId  int `json:"tag_id" db:"tag_id"`
	GameId int `json:"game_id" db:"game_id"`
	Code   int `json:"code" db:"code"`
}

type TblGameTagName struct {
	Tid        int    `json:"tid" db:"tid"`
	Name       string `json:"name" db:"name"`
	State      string `json:"state" db:"state"` // 0:关闭1:开启
	CreatedAt  int    `json:"created_at" db:"created_at"`
	GameType   int    `json:"game_type" db:"game_type"`     // 场馆类型
	PlatformId int    `json:"platform_id" db:"platform_id"` // 游戏ID
}

type TblMemberAdjust struct {
	Id           string `json:"id" cbor:"id" db:"id"`
	Uid          string `json:"uid" cbor:"uid" db:"uid"`
	Username     string `json:"username" cbor:"username" db:"username"`
	Amount       string `json:"amount" cbor:"amount" db:"amount"`
	AdjustMode   int    `json:"adjust_mode" cbor:"adjust_mode" db:"adjust_mode"`
	ApplyRemark  string `json:"apply_remark" cbor:"apply_remark" db:"apply_remark"`
	ReviewRemark string `json:"review_remark" cbor:"review_remark" db:"review_remark"`
	State        int    `json:"state" cbor:"state" db:"state"`
	ApplyAt      int64  `json:"apply_at" cbor:"apply_at" db:"apply_at"`
	ApplyUid     string `json:"apply_uid" cbor:"apply_uid" db:"apply_uid"`
	ApplyName    string `json:"apply_name" cbor:"apply_name" db:"apply_name"`
	ReviewAt     int64  `json:"review_at" cbor:"review_at" db:"review_at"`
	ReviewUid    string `json:"review_uid" cbor:"review_uid" db:"review_uid"`
	ReviewName   string `json:"review_name" cbor:"review_name" db:"review_name"`
	Tester       int    `json:"tester" cbor:"tester" db:"tester"`
}

type TblMessage struct {
	Id       string `json:"id" cbor:"id" db:"id"`                      // id
	Username string `json:"username" cbor:"username" db:"username"`    //会员名
	Title    string `json:"title" cbor:"title" db:"title"`             //标题
	Content  string `json:"content" cbor:"content" db:"content"`       //内容
	IsTop    int    `json:"is_top" cbor:"is_top" db:"is_top"`          //0不置顶 1置顶
	IsVip    int    `json:"is_vip" cbor:"is_vip" db:"is_vip"`          //0非vip站内信 1vip站内信
	Ty       int    `json:"ty" cbor:"ty" db:"ty"`                      //1站内消息 2活动消息
	IsRead   int    `json:"is_read" cbor:"is_read" db:"is_read"`       //是否已读 0未读 1已读
	SendName string `json:"send_name" cbor:"send_name" db:"send_name"` //发送人名
	SendAt   int64  `json:"send_at" cbor:"send_at" db:"send_at"`       //发送时间
}

type TblMemberDepositInfo struct {
	Id            int     `json:"id" db:"id"`
	Uid           int     `json:"uid" db:"uid"`                       // 用户id
	DepositAmount float64 `json:"deposit_amount" db:"deposit_amount"` // 存款金额
	DepositAt     int     `json:"deposit_at" db:"deposit_at"`         // 存款时间
	Flags         int     `json:"flags" db:"flags"`                   // 0 代表上次存款 1-9999代表存款第几次
}

type TblMemberLevelDowngrade struct {
	Id                  int     `json:"id" db:"id"`
	Uid                 int     `json:"uid" db:"uid"`                                     // 会员uid
	Username            string  `json:"username" db:"username"`                           // 会员账号
	BeforeLevel         int     `json:"before_level" db:"before_level"`                   // 调整前会员等级
	AfterLevel          int     `json:"after_level" db:"after_level"`                     // 调整后会员等级
	TotalDeposit        float64 `json:"total_deposit" db:"total_deposit"`                 // 累计存款
	TotalWaterFlow      float64 `json:"total_water_flow" db:"total_water_flow"`           // 累计流水
	RelegationWaterFlow float64 `json:"relegation_water_flow" db:"relegation_water_flow"` // 保级流水
	Ty                  int     `json:"ty" db:"ty"`                                       // 会员等级调整类型 203降级 只能是降级
	CreatedAt           int     `json:"created_at" db:"created_at"`                       // 操作时间
	CreatedUid          int     `json:"created_uid" db:"created_uid"`                     // 操作人uid
	CreatedName         string  `json:"created_name" db:"created_name"`                   // 操作人名
}

type TblMemberLevelRecord struct {
	Id                  int     `json:"id" db:"id"`
	Uid                 int     `json:"uid" db:"uid"`                                     // 会员uid
	Username            string  `json:"username" db:"username"`                           // 会员账号
	BeforeLevel         int     `json:"before_level" db:"before_level"`                   // 调整前会员等级
	AfterLevel          int     `json:"after_level" db:"after_level"`                     // 调整后会员等级
	TotalDeposit        float64 `json:"total_deposit" db:"total_deposit"`                 // 累计存款
	TotalWaterFlow      float64 `json:"total_water_flow" db:"total_water_flow"`           // 累计流水
	RelegationWaterFlow float64 `json:"relegation_water_flow" db:"relegation_water_flow"` // 保级流水
	Ty                  int     `json:"ty" db:"ty"`                                       // 会员等级调整类型 201升级 202保级 203降级 204会员等级恢复
	CreatedAt           int     `json:"created_at" db:"created_at"`                       // 操作时间
	CreatedUid          int     `json:"created_uid" db:"created_uid"`                     // 操作人uid
	CreatedName         string  `json:"created_name" db:"created_name"`                   // 操作人名
	Tester              string  `json:"tester" db:"tester"`                               // 1正式0试玩
}

type TblMemberPlatform struct {
	ID        string `db:"id" json:"id" redis:"id" cbor:"id"`                                 //
	Username  string `db:"username" json:"username" redis:"username" cbor:"username"`         //用户名
	Pid       string `db:"pid" json:"pid" redis:"pid" cbor:"pid"`                             //场馆ID
	Password  string `db:"password" json:"password" redis:"password" cbor:"password"`         //平台密码
	Balance   string `db:"balance" json:"balance" redis:"balance" cbor:"balance"`             //平台余额
	State     int    `db:"state" json:"state" redis:"state" cbor:"state"`                     //状态:1=正常,2=锁定
	CreatedAt uint32 `db:"created_at" json:"created_at" redis:"created_at" cbor:"created_at"` //站点前缀
}

type TblMemberTest struct {
	Id          int    `json:"id" db:"id"`
	Uid         int    `json:"uid" db:"uid"`
	AccountName string `json:"account_name" db:"account_name"`
	Password    string `json:"password" db:"password"`
	OperatorId  int    `json:"operator_id" db:"operator_id"`
	CreateAt    string `json:"create_at" db:"create_at"`
}

type TblMembersTree struct {
	Ancestor   int `json:"ancestor" db:"ancestor"`
	Descendant int `json:"descendant" db:"descendant"`
	Lvl        int `json:"lvl" db:"lvl"`
}

type TblNotices struct {
	Id          string `db:"id" json:"id"`
	Title       string `db:"title" json:"title"`               //标题
	Content     string `db:"content" json:"content"`           //内容
	State       string `db:"state" json:"state"`               //0停用 1启用
	CreatedAt   uint32 `db:"created_at" json:"created_at"`     //创建时间
	CreatedUid  string `db:"created_uid" json:"created_uid"`   //创建人uid
	CreatedName string `db:"created_name" json:"created_name"` //创建人名
}

type TblOperatorDomain struct {
	Id          int    `json:"id" db:"id"`
	DomainUrl   string `json:"domain_url" db:"domain_url"` // 域名
	Uid         int    `json:"uid" db:"uid"`               // 绑定代理id
	Username    string `json:"username" db:"username"`
	State       int    `json:"state" db:"state"`               // 1开启2关闭
	Ty          int    `json:"ty" db:"ty"`                     // 1h5 2pc 3app
	Seq         int    `json:"seq" db:"seq"`                   // 排序
	Remark      string `json:"remark" db:"remark"`             // 备注
	CreatedAt   int    `json:"created_at" db:"created_at"`     // 创建时间
	CreatedName string `json:"created_name" db:"created_name"` // 创建人
	UpdatedAt   int    `json:"updated_at" db:"updated_at"`     // 更新时间
	UpdatedName string `json:"updated_name" db:"updated_name"` // 更新人
}

type TblOperatorInfo struct {
	Id                 string  `json:"id" db:"id"`
	OperatorName       string  `json:"operator_name" db:"operator_name"`
	Password           string  `json:"password" db:"password"`
	ProxyExtendLink    string  `json:"proxy_extend_link" db:"proxy_extend_link"`
	WithdrawRemain     float64 `json:"withdraw_remain" db:"withdraw_remain"`
	RechargeFee        float64 `json:"recharge_fee" db:"recharge_fee"`
	WithdrawFee        float64 `json:"withdraw_fee" db:"withdraw_fee"`
	ApiFee             string  `json:"api_fee" db:"api_fee"`
	SingleUrl          string  `json:"single_url" db:"single_url"`
	YoutubeShareLink   string  `json:"youtube_share_link" db:"youtube_share_link"`
	TwitterShareLink   string  `json:"twitter_share_link" db:"twitter_share_link"`
	TelegramShareLink  string  `json:"telegram_share_link" db:"telegram_share_link"`
	InstagramShareLink string  `json:"instagram_share_link" db:"instagram_share_link"`
	CreatedAt          int64   `json:"created_at" db:"created_at"`
	CreatedUid         string  `json:"created_uid" db:"created_uid"`
	CreatedName        string  `json:"created_name" db:"created_name"`
	UpdatedAt          int64   `json:"updated_at" db:"updated_at"`
	UpdatedUid         string  `json:"updated_uid" db:"updated_uid"`
	UpdatedName        string  `json:"updated_name" db:"updated_name"`
	State              string  `json:"state" db:"state"`
}

type TblPayFactory struct {
	Fid          string `json:"fid" db:"fid" redis:"fid" cbor:"fid"`
	Name         string `json:"name" db:"name" redis:"name" cbor:"name"`
	Url          string `json:"url" db:"url" redis:"url"`
	NotifyUrl    string `json:"notify_url" db:"notify_url" redis:"notify_url"`
	Key          string `json:"key" db:"key" redis:"key"`
	Mchid        string `json:"mchid" db:"mchid" redis:"mchid"`
	AppId        string `json:"app_id" db:"app_id" redis:"app_id"`
	AppKey       string `json:"app_key" db:"app_key" redis:"app_key"`
	PayCode      string `json:"pay_code" db:"pay_code" redis:"pay_code"`
	Fmax         string `json:"fmax" db:"fmax" redis:"fmax" cbor:"fmax"`
	Fmin         string `json:"fmin" db:"fmin" redis:"fmin" cbor:"fmin"`
	AmountList   string `json:"amount_list" db:"amount_list" redis:"amount_list" cbor:"amount_list"`
	ShowName     string `json:"show_name" db:"show_name" cbor:"show_name"`
	State        string `json:"state" db:"state" cbor:"state"`
	PayRate      string `json:"pay_rate" db:"pay_rate" cbor:"pay_rate"`
	Ty           int    `json:"ty" db:"ty" cbor:"ty"`
	Automatic    string `json:"automatic" db:"automatic" cbor:"automatic"`
	Sort         int    `json:"sort" cbor:"sort" db:"sort"`
	CountryCode  string `json:"country_code" db:"country_code" cbor:"country_code"`
	CurrencyCode string `json:"currency_code" db:"currency_code" cbor:"currency_code"`
	Type         string `json:"type" db:"type" cbor:"type"`
}

type TblPddTurntableInfo struct {
	Uid          string  `json:"uid"  db:"uid"`                  // uid
	UnuseChance  int     `json:"unuse_chance" db:"unuse_chance"` // 剩余抽奖次数
	UsedChance   int     `json:"used_chance" db:"used_chance"`   // 已用抽奖次数
	Amount       float64 `json:"amount" db:"amount"`             // 奖励金额
	HandAmount   float64 `json:"hand_amount" db:"hand_amount"`   // 手动奖励金额
	RewardAmount float64 `json:"reward_amount" db:"-"`           // 奖励总金额
	TotAmount    float64 `json:"tot_amount" db:"tot_amount"`     // 已领取的总金额
	Enabled      int     `json:"enabled" db:"enabled"`           // 0停止 1正常
	CreatedAt    int64   `json:"created_at" db:"created_at"`     // 记录时间
	UpdatedAt    int64   `json:"updated_at" db:"updated_at"`     // 更新时间
}

type TblPddTurntableHistory struct {
	Id           string  `json:"id" db:"id"`
	Uid          string  `json:"uid" db:"uid"`                     // 用户ID
	Amount       float64 `json:"amount" db:"amount"`               // 奖励金额
	BeforeAmount float64 `json:"before_amount" db:"before_amount"` // 账变前的金额
	AfterAmount  float64 `json:"after_amount" db:"after_amount"`   // 账变后的金额
	CreatedAt    int64   `json:"created_at" db:"created_at"`
	Type         int     `json:"type" db:"type"` // 增加类型 1 大随机金额 2 小随机金额 3 1000元 4 50元 5 1元 6 获得随机次数 7 直接领奖 8 无奖励 9 后台增加
	Remark       string  `json:"remark" db:"remark"`
}

type TblPddTurntableReview struct {
	Id           string  `json:"id" db:"id"`
	Uid          string  `json:"uid" db:"uid"`                     // 申请人uid
	Username     string  `json:"username" db:"username"`           // 申请人
	Amount       float64 `json:"amount" db:"amount"`               // 领取金额
	ReviewRemark string  `json:"review_remark" db:"review_remark"` // 审核备注
	State        int     `json:"state" db:"state"`                 // 状态:1=审核中,2=审核通过,3=审核未通过
	ApplyAt      int64   `json:"apply_at" db:"apply_at"`           // 申请时间
	ReviewAt     int64   `json:"review_at" db:"review_at"`         // 审核时间
	ReviewUid    int     `json:"review_uid" db:"review_uid"`       // 审核人uid
	ReviewName   string  `json:"review_name" db:"review_name"`     // 审核人
}

type TblPlatforms struct {
	ID          string `db:"id" json:"id" cbor:"id"`
	Name        string `db:"name" json:"name" cbor:"name"`
	GameType    int    `db:"game_type" json:"game_type" cbor:"game_type"`
	State       int    `db:"state" json:"state" cbor:"state"`
	Maintained  int    `db:"maintained" json:"maintained" cbor:"maintained"`
	Seq         int    `db:"seq" json:"seq" cbor:"seq"`
	Logo        string `db:"logo" json:"logo" cbor:"logo"`
	CreatedAt   int32  `db:"created_at" json:"created_at" cbor:"created_at"`
	UpdatedAt   int32  `db:"updated_at" json:"updated_at" cbor:"updated_at"`
	UpdatedUID  string `db:"updated_uid" json:"updated_uid" cbor:"updated_uid"`
	UpdatedName string `db:"updated_name" json:"updated_name" cbor:"updated_name"`
}

type TblPromoDeposit struct {
	Id        int     `json:"id" cbor:"id" db:"id"`
	Name      string  `json:"name" cbor:"name" db:"name"`                   //配置名称
	Bonus     float64 `json:"bonus" cbor:"bonus" db:"bonus"`                //奖金比例
	Flow      float64 `json:"flow" cbor:"flow" db:"flow"`                   //流水倍数
	MaxAmount float64 `json:"max_amount" cbor:"max_amount" db:"max_amount"` //最大金额
	MinAmount float64 `json:"min_amount" cbor:"min_amount" db:"min_amount"` //最小金额
	Ty        int     `json:"ty" cbor:"ty" db:"ty"`                         //1首存2次存
}

type TblPromoInspection struct {
	Id               string  `json:"id" db:"id" `
	Uid              string  `json:"uid" db:"uid"`
	Username         string  `json:"username" db:"username"`
	ParentId         string  `json:"parent_id" db:"parent_id"`
	ParentName       string  `json:"parent_name" db:"parent_name"`
	Level            int     `json:"level" db:"level"`
	Ty               int     `json:"ty" db:"ty"`
	State            int     `json:"state" db:"state"`
	CapitalAmount    float64 `json:"capital_amount" db:"capital_amount"`
	FlowMultiple     float64 `json:"flow_multiple" db:"flow_multiple"`
	FlowAmount       float64 `json:"flow_amount" db:"flow_amount"`
	FinishedAmount   float64 `json:"finished_amount" db:"finished_amount"`
	UnfinishedAmount float64 `json:"unfinished_amount" db:"unfinished_amount"`
	CreatedAt        int64   `json:"created_at" db:"created_at"`
	StartAt          int64   `json:"start_at" db:"start_at"`
	FinishAt         int64   `json:"finish_at" db:"finish_at"`
	BillNo           string  `json:"bill_no" db:"bill_no"`
	Remark           string  `json:"remark" db:"remark"`
}

type TblPromoInviteRecord struct {
	Id             string  `db:"id" json:"id"`
	Uid            string  `db:"uid" json:"uid"`
	Username       string  `db:"username" json:"username"`
	Lvl            int     `db:"lvl" json:"lvl"`                           //123 一级二级三级
	ChildUid       string  `db:"child_uid" json:"child_uid"`               //下级的uid
	ChildUsername  string  `db:"child_username" json:"child_username"`     //下级的账号
	FirstDepositAt int     `db:"first_deposit_at" json:"first_deposit_at"` //首存时间
	DepositAmount  float64 `db:"deposit_amount" json:"deposit_amount"`     //存款金额
	BonusAmount    float64 `db:"bonus_amount" json:"bonus_amount"`         //奖金
	CreatedAt      uint32  `db:"created_at" json:"created_at"`             //注册时间
	State          int     `db:"state" json:"state"`                       //状态1注册未充值2充值未结算3已结算4过期作废
}

type TblPromoSignConfig struct {
	Vip             int    `json:"vip" cbor:"vip" db:"vip"`                                           //会员等级
	Sign1Amount     string `json:"sign1_amount" cbor:"sign1_amount" db:"sign1_amount"`                //第一天签到彩金金额
	Sign2Amount     string `json:"sign2_amount" cbor:"sign2_amount" db:"sign2_amount"`                //第二天签到彩金金额
	Sign3Amount     string `json:"sign3_amount" cbor:"sign3_amount" db:"sign3_amount"`                //第三天签到彩金金额
	Sign4Amount     string `json:"sign4_amount" cbor:"sign4_amount" db:"sign4_amount"`                //第四天签到彩金金额
	Sign5Amount     string `json:"sign5_amount" cbor:"sign5_amount" db:"sign5_amount"`                //第五天签到彩金金额
	Sign6Amount     string `json:"sign6_amount" cbor:"sign6_amount" db:"sign6_amount"`                //第六天签到彩金金额
	Sign7Amount     string `json:"sign7_amount" cbor:"sign7_amount" db:"sign7_amount"`                //第七天签到彩金金额
	SignWeekAmount  string `json:"sign_week_amount" cbor:"sign_week_amount" db:"sign_week_amount"`    //周签到彩金金额
	SignMonthAmount string `json:"sign_month_amount" cbor:"sign_month_amount" db:"sign_month_amount"` //月签到彩金金额
}

type TblPromoSignRecord struct {
	UID       string `json:"uid" cbor:"uid" db:"uid"`
	Username  string `json:"username" cbor:"username" db:"username"`          //会员名
	Vip       int    `json:"vip" cbor:"vip" db:"vip"`                         //签到时的会员等级
	SignWeek1 string `json:"sign_week_1" cbor:"sign_week_1" db:"sign_week_1"` //周第一天签到
	SignWeek2 string `json:"sign_week_2" cbor:"sign_week_2" db:"sign_week_2"` //周第二天签到
	SignWeek3 string `json:"sign_week_3" cbor:"sign_week_3" db:"sign_week_3"` //周第三天签到
	SignWeek4 string `json:"sign_week_4" cbor:"sign_week_4" db:"sign_week_4"` //周第四天签到
	SignWeek5 string `json:"sign_week_5" cbor:"sign_week_5" db:"sign_week_5"` //周第五天签到
	SignWeek6 string `json:"sign_week_6" cbor:"sign_week_6" db:"sign_week_6"` //周第六天签到
	SignWeek7 string `json:"sign_week_7" cbor:"sign_week_7" db:"sign_week_7"` //周第七天签到
	Sign1     string `json:"sign1" cbor:"sign1" db:"sign1"`                   //第一天签到
	Sign2     string `json:"sign2" cbor:"sign2" db:"sign2"`                   //第二天签到
	Sign3     string `json:"sign3" cbor:"sign3" db:"sign3"`                   //第三天签到
	Sign4     string `json:"sign4" cbor:"sign4" db:"sign4"`                   //第四天签到
	Sign5     string `json:"sign5" cbor:"sign5" db:"sign5"`                   //第五天签到
	Sign6     string `json:"sign6" cbor:"sign6" db:"sign6"`                   //第六天签到
	Sign7     string `json:"sign7" cbor:"sign7" db:"sign7"`                   //第七天签到
	Sign8     string `json:"sign8" cbor:"sign8" db:"sign8"`                   //第八天签到
	Sign9     string `json:"sign9" cbor:"sign9" db:"sign9"`                   //第九天签到
	Sign10    string `json:"sign10" cbor:"sign10" db:"sign10"`                //第十天签到
	Sign11    string `json:"sign11" cbor:"sign11" db:"sign11"`                //第十一天签到
	Sign12    string `json:"sign12" cbor:"sign12" db:"sign12"`                //第十二天签到
	Sign13    string `json:"sign13" cbor:"sign13" db:"sign13"`                //第十三天签到
	Sign14    string `json:"sign14" cbor:"sign14" db:"sign14"`                //第十四天签到
	Sign15    string `json:"sign15" cbor:"sign15" db:"sign15"`                //第十五天签到
	Sign16    string `json:"sign16" cbor:"sign16" db:"sign16"`                //第十六天签到
	Sign17    string `json:"sign17" cbor:"sign17" db:"sign17"`                //第十七天签到
	Sign18    string `json:"sign18" cbor:"sign18" db:"sign18"`                //第十八天签到
	Sign19    string `json:"sign19" cbor:"sign19" db:"sign19"`                //第十九天签到
	Sign20    string `json:"sign20" cbor:"sign20" db:"sign20"`                //第二十天签到
	Sign21    string `json:"sign21" cbor:"sign21" db:"sign21"`                //第二十一天签到
	Sign22    string `json:"sign22" cbor:"sign22" db:"sign22"`                //第二十二天签到
	Sign23    string `json:"sign23" cbor:"sign23" db:"sign23"`                //第二十三天签到
	Sign24    string `json:"sign24" cbor:"sign24" db:"sign24"`                //第二十四天签到
	Sign25    string `json:"sign25" cbor:"sign25" db:"sign25"`                //第二十五天签到
	Sign26    string `json:"sign26" cbor:"sign26" db:"sign26"`                //第二十六天签到
	Sign27    string `json:"sign27" cbor:"sign27" db:"sign27"`                //第二十七天签到
	Sign28    string `json:"sign28" cbor:"sign28" db:"sign28"`                //第二十八天签到
	Sign29    string `json:"sign29" cbor:"sign29" db:"sign29"`                //第二十九天签到
	Sign30    string `json:"sign30" cbor:"sign30" db:"sign30"`                //第三十天签到
	LastSign  string `json:"last_sign" cbor:"last_sign" db:"last_sign"`       //最后一次签到时间yyyy-mm-dd
}

type TblPromoSignRewardRecord struct {
	ID        string `json:"id" db:"id" cbor:"id"`                         //id
	UID       string `json:"uid" db:"uid" cbor:"uid"`                      //uid
	Username  string `json:"username" db:"username" cbor:"username"`       //会员名
	Vip       int    `json:"vip" db:"vip" cbor:"vip"`                      //vip
	Day       int    `json:"day" db:"day" cbor:"day"`                      //第几天
	MonthDay  int    `json:"month_day" db:"month_day" cbor:"month_day"`    //月签到第几天
	Amount    string `json:"amount" db:"amount" cbor:"amount"`             //奖金金额
	CreatedAt int64  `json:"created_at" db:"created_at" cbor:"created_at"` //记录时间
}

type TblPromoTreasureConfig struct {
	ID          string `json:"id" cbor:"id" db:"id"`
	InviteNum   uint32 `json:"invite_num" cbor:"invite_num" db:"invite_num"`       //邀请人数
	Amount      string `json:"amount" cbor:"amount" db:"amount"`                   //宝箱金额
	TotalAmount string `json:"total_amount" cbor:"total_amount" db:"total_amount"` //累计宝箱金额
}

type TblPromoTreasureRecord struct {
	ID        string `json:"id" cbor:"id" db:"id"`
	UID       string `json:"uid" cbor:"uid" db:"uid"`
	Username  string `json:"username" cbor:"username" db:"username"`       //会员名
	InviteNum int    `json:"invite_num" cbor:"invite_num" db:"invite_num"` //邀请人数
	Amount    string `json:"amount" cbor:"amount" db:"amount"`             //宝箱金额
	CreatedAt int64  `json:"created_at" cbor:"created_at" db:"created_at"` //记录时间
}

type TblPromoWeekbetRecord struct {
	Id              string  `json:"id" db:"id"`
	Uid             string  `json:"uid" db:"uid"`
	ReportTime      int64   `json:"report_time" db:"report_time"`
	ValidBetAmount  float64 `json:"valid_bet_amount" db:"valid_bet_amount"`
	WaitBonusAmount float64 `json:"wait_bonus_amount" db:"wait_bonus_amount"`
	BonusAmount     float64 `json:"bonus_amount" db:"bonus_amount"`
	State           int     `json:"state" db:"state"`
	UpdatedAt       int64   `json:"updated_at" db:"updated_at"`
	Remark          string  `json:"remark" db:"remark"`
	PayAt           int64   `json:"pay_at" db:"pay_at"`
}

type TblPromoWeekBetConfig struct {
	Id          string  `json:"id" db:"id" cbor:"id"`
	FlowAmount  float64 `json:"flow_amount" db:"flow_amount" cbor:"flow_amount"`
	BonusAmount float64 `json:"bonus_amount" db:"bonus_amount" cbor:"bonus_amount"`
	UpdatedAt   int64   `json:"updated_at" db:"updated_at" cbor:"updated_at"`
	UpdatedName string  `json:"updated_name" db:"updated_name" cbor:"updated_name"`
}

type TblRebateRecord struct {
	ID           string `json:"id" db:"id" cbor:"id"`
	RowId        string `json:"row_id" db:"row_id" cbor:"row_id"`                      //注单表row_id
	Uid          string `json:"uid" db:"uid" cbor:"uid"`                               //用户ID(返水会员uid)
	Username     string `json:"username" db:"username" cbor:"username"`                //用户名(返水会员名)
	SubUid       string `json:"sub_uid" db:"sub_uid" cbor:"sub_uid"`                   //下级用户ID(投注会员uid)
	SubName      string `json:"sub_name" db:"sub_name" cbor:"sub_name"`                //下级用户名(投注会员名)
	PlatformId   string `json:"platform_id" db:"platform_id" cbor:"platform_id"`       //场馆id
	CashType     int    `json:"cash_type" db:"cash_type" cbor:"cash_type"`             //203 下级返水（1级）204下级返水（2级）205下级返水（3级）
	Amount       string `json:"amount" db:"amount" cbor:"amount"`                      //用户填写的转换金额
	RebateAmount string `json:"rebate_amount" db:"rebate_amount" cbor:"rebate_amount"` //用户填写的转换金额
	Rate         string `json:"rate" db:"rate" cbor:"rate"`                            //返水比例
	SettleTime   int64  `json:"settle_time" db:"settle_time" cbor:"settle_time"`       //注单结算时间
	CreatedAt    int64  `json:"created_at" db:"created_at" cbor:"created_at"`          //创建时间
}

type TblReportAgency struct {
	Id                string  `db:"id" json:"id"`
	MemCount          int     `db:"mem_count" json:"mem_count"`                     //人数
	DepositMemCount   int     `db:"deposit_mem_count" json:"deposit_mem_count"`     //充值人数
	FirstDepositBonus float64 `db:"first_deposit_bonus" json:"first_deposit_bonus"` //首充奖励
	ValidBetAmount    float64 `db:"valid_bet_amount" json:"valid_bet_amount"`       //流水
	RebateAmount      float64 `db:"rebate_amount" json:"rebate_amount"`             //下级返水
	TotalAmount       float64 `db:"total_amount" json:"total_amount"`               //总佣金
	Ty                int     `db:"ty" json:"ty"`
	ReportTime        int64   `db:"report_time" json:"report_time"`
	Lvl               string  `db:"lvl" json:"lvl"`
	Uid               string  `db:"uid" json:"uid"`
	Username          string  `db:"username" json:"username"`
}

type TblReportDaily struct {
	Id               int64   `json:"id" db:"id"`
	BusinessId       string  `json:"business_id" db:"business_id"`               // 业务员id
	OperatorId       string  `json:"operator_id" db:"operator_id"`               // 渠道id
	DepositNum       float64 `json:"deposit_num" db:"deposit_num"`               // 充值人数
	Deposit          float64 `json:"deposit" db:"deposit"`                       // 充值数量
	FirstDepositNum  float64 `json:"first_deposit_num" db:"first_deposit_num"`   // 首充人数
	FirstDeposit     float64 `json:"first_deposit" db:"first_deposit"`           // 首充数量
	WithdrawNum      float64 `json:"withdraw_num" db:"withdraw_num"`             // 提现人数
	Withdraw         float64 `json:"withdraw" db:"withdraw"`                     // 提现数量
	Running          float64 `json:"running" db:"running"`                       // 流水
	GameRound        int32   `json:"game_round" db:"game_round"`                 // 游戏局数
	GameWinlost      float64 `json:"game_winlost" db:"game_winlost"`             // 游戏赢亏
	TurntableBonus   float64 `json:"turntable_bonus" db:"turntable_bonus"`       // 转盘奖励
	ProxyInviteBonus float64 `json:"proxy_invite_bonus" db:"proxy_invite_bonus"` // 代理邀请奖励
	CreateDate       string  `json:"create_date" db:"create_date"`               // 时间
}

type TblReportGame struct {
	Id                  string  `json:"id" db:"id"`
	ReportTime          int64   `json:"report_time" db:"report_time"`
	ReportType          int64   `json:"report_type" db:"report_type"`
	ApiType             int64   `json:"api_type" db:"api_type"`
	Prefix              string  `json:"prefix" db:"prefix"`
	MemCount            int64   `json:"mem_count" db:"mem_count"`
	BetCount            int64   `json:"bet_count" db:"bet_count"`
	BetAmount           float64 `json:"bet_amount" db:"bet_amount"`
	ValidBetAmount      float64 `json:"valid_bet_amount" db:"valid_bet_amount"`
	CompanyNetAmount    float64 `json:"company_net_amount" db:"company_net_amount"`
	AvgBetAmount        float64 `json:"avg_bet_amount" db:"avg_bet_amount"`
	AvgValidBetAmount   float64 `json:"avg_valid_bet_amount" db:"avg_valid_bet_amount"`
	AvgCompanyNetAmount float64 `json:"avg_company_net_amount" db:"avg_company_net_amount"`
	Presettle           float64 `json:"presettle" db:"presettle"`
	ProfitRate          float64 `json:"profit_rate" db:"profit_rate"`
	RebateAmount        float64 `json:"rebate_amount" db:"rebate_amount"`
}

type TblReportGameDaily struct {
	Id          int     `json:"id" db:"id"`                     // 主键
	BusinessId  int     `json:"business_id" db:"business_id"`   // 业务员ID
	OperatorId  int     `json:"operator_id" db:"operator_id"`   // 渠道ID
	GameId      int     `json:"game_id" db:"game_id"`           // 接口平台 如 pp pg，对应game_list id
	Running     float64 `json:"running" db:"running"`           // 流水
	GameRound   int     `json:"game_round" db:"game_round"`     // 游戏局数
	GameWinlost float64 `json:"game_winlost" db:"game_winlost"` // 游戏赢亏
	GameTax     float64 `json:"game_tax" db:"game_tax"`         // 税收
	CreateDate  string  `json:"create_date" db:"create_date"`   // 时间
}

type TblReportPlatform struct {
	Id                    string  `json:"id" db:"id" cbor:"id"`
	ReportTime            int64   `json:"report_time" db:"report_time" cbor:"report_time"`
	ReportType            int     `json:"report_type" db:"report_type" cbor:"report_type"`
	ReportMonth           int64   `json:"report_month" db:"report_month" cbor:"report_month"`
	RegistCount           int64   `json:"regist_count" db:"regist_count" cbor:"regist_count"`
	ActiveCount           int64   `json:"active_count" db:"active_count" cbor:"active_count"`
	EfficientActiveCount  int64   `json:"efficient_active_count" db:"efficient_active_count" cbor:"efficient_active_count"`
	FirstDepositCount     int64   `json:"first_deposit_count" db:"first_deposit_count" cbor:"first_deposit_count"`
	SecondDepositCount    int64   `json:"second_deposit_count" db:"second_deposit_count" cbor:"second_deposit_count"`
	ThirdDepositCount     int64   `json:"third_deposit_count" db:"third_deposit_count" cbor:"third_deposit_count"`
	DepositCount          int64   `json:"deposit_count" db:"deposit_count" cbor:"deposit_count"`
	WithdrawalCount       int64   `json:"withdrawal_count" db:"withdrawal_count" cbor:"withdrawal_count"`
	ConversionRate        float64 `json:"conversion_rate" db:"conversion_rate" cbor:"conversion_rate"`
	FirstDepositAmount    float64 `json:"first_deposit_amount" db:"first_deposit_amount" cbor:"first_deposit_amount"`
	SecondDepositAmount   float64 `json:"second_deposit_amount" db:"second_deposit_amount" cbor:"second_deposit_amount"`
	ThirdDepositAmount    float64 `json:"third_deposit_amount" db:"third_deposit_amount" cbor:"third_deposit_amount"`
	AvgFirstDepositAmount float64 `json:"avg_first_deposit_amount" db:"avg_first_deposit_amount" cbor:"avg_first_deposit_amount"`
	DepositMemCount       int64   `json:"deposit_mem_count" db:"deposit_mem_count" cbor:"deposit_mem_count"`
	WithdrawalMemCount    int64   `json:"withdrawal_mem_count" db:"withdrawal_mem_count" cbor:"withdrawal_mem_count"`
	DepositAmount         float64 `json:"deposit_amount" db:"deposit_amount" cbor:"deposit_amount"`
	WithdrawalAmount      float64 `json:"withdrawal_amount" db:"withdrawal_amount" cbor:"withdrawal_amount"`
	DepositWithdrawalSub  float64 `json:"deposit_withdrawal_sub" db:"deposit_withdrawal_sub" cbor:"deposit_withdrawal_sub"`
	DepositWithdrawalRate float64 `json:"deposit_withdrawal_rate" db:"deposit_withdrawal_rate" cbor:"deposit_withdrawal_rate"`
	BetMemCount           int64   `json:"bet_mem_count" db:"bet_mem_count" cbor:"bet_mem_count"`
	BetNumCount           int64   `json:"bet_num_count" db:"bet_num_count" cbor:"bet_num_count"`
	BetAmount             float64 `json:"bet_amount" db:"bet_amount" cbor:"bet_amount"`
	ValidBetAmount        float64 `json:"valid_bet_amount" db:"valid_bet_amount" cbor:"valid_bet_amount"`
	CompanyNetAmount      float64 `json:"company_net_amount" db:"company_net_amount" cbor:"company_net_amount"`
	AvgCompanyNetAmount   float64 `json:"avg_company_net_amount" db:"avg_company_net_amount" cbor:"avg_company_net_amount"`
	ProfitAmount          float64 `json:"profit_amount" db:"profit_amount" cbor:"profit_amount"`
	Presettle             float64 `json:"presettle" db:"presettle" cbor:"presettle"`
	CompanyRevenue        float64 `json:"company_revenue" db:"company_revenue" cbor:"company_revenue"`
	Uids                  string  `json:"uids" db:"uids"`
}

type TblReportProxyDaily struct {
	Id               int     `json:"id" db:"id"`                                 // 主键
	ProxyId          int     `json:"proxy_id" db:"proxy_id"`                     // 业务员ID
	Level            int     `json:"level" db:"level"`                           // 渠道ID
	DepositNum       int     `json:"deposit_num" db:"deposit_num"`               // 充值人数
	Deposit          float64 `json:"deposit" db:"deposit"`                       // 充值数量
	FirstDepositNum  int     `json:"first_deposit_num" db:"first_deposit_num"`   // 首充人数
	FirstDeposit     float64 `json:"first_deposit" db:"first_deposit"`           // 首充数量
	WithdrawNum      int     `json:"withdraw_num" db:"withdraw_num"`             // 提现人数
	Withdraw         float64 `json:"withdraw" db:"withdraw"`                     // 提现数量
	Running          float64 `json:"running" db:"running"`                       // 流水
	GameRound        float64 `json:"game_round" db:"game_round"`                 // 游戏局数
	GameTax          float64 `json:"game_tax" db:"game_tax"`                     // 税收
	GameWinlost      float64 `json:"game_winlost" db:"game_winlost"`             // 游戏赢亏
	TurntableBonus   float64 `json:"turntable_bonus" db:"turntable_bonus"`       // 转盘奖励
	ProxyInviteBonus float64 `json:"proxy_invite_bonus" db:"proxy_invite_bonus"` // 代理邀请奖励
	CreateDate       string  `json:"create_date" db:"create_date"`               // 时间
}

type TblReportProxyDaily1 struct {
	Uid                 int     `json:"uid" db:"uid"`
	Lv1Tax              float64 `json:"lv1_tax" db:"lv_1_tax"`
	Lv1PersonCount      int     `json:"lv1_person_count" db:"lv_1_person_count"`
	Lv1Running          float64 `json:"lv1_running" db:"lv_1_running"`
	Lv1Deposit          float64 `json:"lv1_deposit" db:"lv_1_deposit"`
	Lv1ValidInviteCount int     `json:"lv1_valid_Invite_count" db:"lv_1_valid_invite_count"`
	Lv2Tax              float64 `json:"lv2_tax" db:"lv_2_tax"`
	Lv2PersonCount      int     `json:"lv2_person_count" db:"lv_2_person_count"`
	Lv2Running          float64 `json:"lv2_running" db:"lv_2_running"`
	Lv2Deposit          float64 `json:"lv2_deposit" db:"lv_2_deposit"`
	Lv2ValidInviteCount int     `json:"lv2_valid_Invite_count" db:"lv_2_valid_invite_count"`
	Lv3Tax              float64 `json:"lv3_tax" db:"lv_3_tax"`
	Lv3PersonCount      int     `json:"lv3_person_count" db:"lv_3_person_count"`
	Lv3Running          float64 `json:"lv3_running" db:"lv_3_running"`
	Lv3Deposit          float64 `json:"lv3_deposit" db:"lv_3_deposit"`
	Lv3ValidInviteCount int     `json:"lv3_valid_Invite_count" db:"lv_3_valid_invite_count"`
	CreatedAt           string  `json:"created_at" db:"created_at"`
}

type TblReportUser struct {
	Id                     string  `json:"id" db:"id"`
	Ty                     int     `json:"ty" db:"ty"`
	ReportTime             int64   `json:"report_time" db:"report_time"`
	Uid                    string  `json:"uid" db:"uid"`
	TotalCommissions       float64 `json:"total_commissions" db:"total_commissions"`                 //历史总佣金
	WaitCommissions        float64 `json:"wait_commissions" db:"wait_commissions"`                   //待结算佣金
	TotalTeamNum           int     `json:"total_team_num" db:"total_team_num"`                       //团队总人数
	DepositWithdrawDiff    float64 `json:"deposit_withdraw_diff" db:"deposit_withdraw_diff"`         //团队存取查
	DirectPushNum          int     `json:"direct_push_num" db:"direct_push_num"`                     //直推人数
	DirectPushDepositNum   int     `json:"direct_push_deposit_num" db:"direct_push_deposit_num"`     //直推充值人数
	DirectPushFdBonus      float64 `json:"direct_push_fd_bonus" db:"direct_push_fd_bonus"`           //直推首充奖励
	DirectPushDeposit      float64 `json:"direct_push_deposit" db:"direct_push_deposit"`             //直推充值
	DirectPushFlow         float64 `json:"direct_push_flow" db:"direct_push_flow"`                   //直推流水
	DirectPushFlowBonus    float64 `json:"direct_push_flow_bonus" db:"direct_push_flow_bonus"`       //直推流水奖励
	DirectPushDwDiff       float64 `json:"direct_push_dw_diff" db:"direct_push_dw_diff"`             //直推充提差
	DirectPushWaitFlow     float64 `json:"direct_push_wait_flow" db:"direct_push_wait_flow"`         //待结算直推流水
	DirectPushWaitBonus    float64 `json:"direct_push_wait_bonus" db:"direct_push_wait_bonus"`       //待结算直推流水奖励
	LvlSecondNum           int     `json:"lvl_second_num" db:"lvl_second_num"`                       //二级人数
	LvlSecondDepositNum    int     `json:"lvl_second_deposit_num" db:"lvl_second_deposit_num"`       //二级充值人数
	LvlSecondDepositAmount float64 `json:"lvl_second_deposit_amount" db:"lvl_second_deposit_amount"` ///二级充值金额
	LvlSecondFlow          float64 `json:"lvl_second_flow" db:"lvl_second_flow"`                     //二级流水
	LvlSecondFlowBonus     float64 `json:"lvl_second_flow_bonus" db:"lvl_second_flow_bonus"`         //二级流水奖励
	LvlSecondWaitFlow      float64 `json:"lvl_second_wait_flow" db:"lvl_second_wait_flow"`           //二级待结算流水
	LvlSecondWaitBonus     float64 `json:"lvl_second_wait_bonus" db:"lvl_second_wait_bonus"`         //二级待结算流水奖励
	LvlSecondDwDiff        float64 `json:"lvl_second_dw_diff" db:"lvl_second_dw_diff"`               //二级充提差
	LvlThreeNum            int     `json:"lvl_three_num" db:"lvl_three_num"`                         //三级人数
	LvlThreeDepositNum     int     `json:"lvl_three_deposit_num" db:"lvl_three_deposit_num"`         //三级充值人数
	LvlThreeDepositAmount  float64 `json:"lvl_three_deposit_amount" db:"lvl_three_deposit_amount"`   //三级充值金额
	LvlThreeFlow           float64 `json:"lvl_three_flow" db:"lvl_three_flow"`                       //三级流水
	LvlThreeFlowBonus      float64 `json:"lvl_three_flow_bonus" db:"lvl_three_flow_bonus"`           //三级流水奖励
	LvlThreeWaitFlow       float64 `json:"lvl_three_wait_flow" db:"lvl_three_wait_flow"`             //三级待结算流水
	LvlThreeWaitBonus      float64 `json:"lvl_three_wait_bonus" db:"lvl_three_wait_bonus"`           //三级待结算流水奖励
	LvlThreeDwDiff         float64 `json:"lvl_three_dw_diff" db:"lvl_three_dw_diff"`                 //三级充提差
	WithdrawAmount         float64 `json:"withdraw_amount" db:"withdraw_amount"`                     //提现金额
	Username               string  `json:"username" db:"username"`                                   //用户名
	CreatedAt              uint32  `json:"created_at" db:"created_at"`                               //注册时间
	CreatedIp              string  `json:"created_ip" db:"created_ip"`                               //注册ip
}

type TblReportUserDaily struct {
	Uid              int     `json:"uid" db:"uid"`                               // 用户ID
	Deposit          float64 `json:"deposit" db:"deposit"`                       // 充值
	FirstDeposit     float64 `json:"first_deposit" db:"first_deposit"`           // 首充数量
	Withdraw         float64 `json:"withdraw" db:"withdraw"`                     // 提现
	Running          float64 `json:"running" db:"running"`                       // 流水
	GameRound        int     `json:"game_round" db:"game_round"`                 // 游戏局数
	GameWinlost      float64 `json:"game_winlost" db:"game_winlost"`             // 游戏赢亏
	GameTax          float64 `json:"game_tax" db:"game_tax"`                     // 税收
	TurntableBonus   float64 `json:"turntable_bonus" db:"turntable_bonus"`       // 转盘奖励
	ProxyInviteBonus float64 `json:"proxy_invite_bonus" db:"proxy_invite_bonus"` // 代理邀请奖励
	CreatedAt        string  `json:"created_at" db:"created_at"`                 // 数据
}

type TblTransactionTypes struct {
	ID     string `json:"id" cbor:"id" db:"id"`
	Name   string `json:"name" cbor:"name" db:"name"`          //帐变中文名
	EnName string `json:"en_name" cbor:"en_name" db:"en_name"` //帐变英文名
}

type TblWhitelist struct {
	Id          string `db:"id" json:"id"`
	Ip          string `db:"ip" json:"ip"`                     //ip
	Remark      string `db:"remark" json:"remark"`             //备注
	CreatedAt   uint32 `db:"created_at" json:"created_at"`     //添加时间
	CreatedUid  string `db:"created_uid" json:"created_uid"`   //添加人uid
	CreatedName string `db:"created_name" json:"created_name"` //添加人名
	Area        string `db:"area" json:"area"`                 //区域 国家 省 市
}

type TblWithdraw struct {
	ID             string  `db:"id" json:"id" cbor:"id"`                                        //
	OID            string  `db:"oid" json:"oid" cbor:"oid"`                                     //三方订单
	UID            string  `db:"uid" json:"uid" cbor:"uid"`                                     //用户ID
	Username       string  `db:"username" json:"username" cbor:"username"`                      //用户名
	TopId          string  `db:"top_id" json:"top_id" cbor:"top_id"`                            //总代uid
	TopName        string  `db:"top_name" json:"top_name" cbor:"top_name"`                      //总代代理
	ParentId       string  `db:"parent_id" json:"parent_id" cbor:"parent_id"`                   //上级uid
	ParentName     string  `db:"parent_name" json:"parent_name" cbor:"parent_name"`             //上级代理
	FID            string  `db:"fid" json:"fid" cbor:"fid"`                                     //通道id
	Fname          string  `db:"fname" json:"fname" cbor:"fname"`                               //通道名称
	Amount         float64 `db:"amount" json:"amount" cbor:"amount"`                            //金额
	Fee            float64 `db:"fee" json:"fee" cbor:"fee"`                                     //手续费
	State          int     `db:"state" json:"state" cbor:"state"`                               //0:待确认:1存款成功2:已取消
	Automatic      int     `db:"automatic" json:"automatic" cbor:"automatic"`                   //1:自动转账2:脚本确认3:人工确认
	PixAccount     string  `db:"pix_account" json:"pix_account" cbor:"pix_account"`             //银行名
	RealName       string  `db:"real_name" json:"real_name" cbor:"real_name"`                   //持卡人姓名
	PixId          string  `db:"pix_id" json:"pix_id" cbor:"pix_id"`                            //银行卡号
	CreatedAt      int64   `db:"created_at" json:"created_at" cbor:"created_at"`                //
	ConfirmAt      int64   `db:"confirm_at" json:"confirm_at" cbor:"confirm_at"`                //确认时间
	ConfirmUID     string  `db:"confirm_uid" json:"confirm_uid" cbor:"confirm_uid"`             //确认人uid
	ConfirmName    string  `db:"confirm_name" json:"confirm_name" cbor:"confirm_name"`          //确认人名
	ReviewRemark   string  `db:"review_remark" json:"review_remark" cbor:"review_remark"`       //确认人名
	WithdrawAt     int64   `db:"withdraw_at" json:"withdraw_at" cbor:"withdraw_at"`             //三方场馆ID
	WithdrawRemark string  `db:"withdraw_remark" json:"withdraw_remark" cbor:"withdraw_remark"` //确认人名
	WithdrawUID    string  `db:"withdraw_uid" json:"withdraw_uid" cbor:"withdraw_uid"`          //确认人uid
	WithdrawName   string  `db:"withdraw_name" json:"withdraw_name" cbor:"withdraw_name"`       //确认人名
	HangUpUID      string  `db:"hang_up_uid" json:"hang_up_uid" cbor:"hang_up_uid"`             // 挂起人uid
	HangUpRemark   string  `db:"hang_up_remark" json:"hang_up_remark" cbor:"hang_up_remark"`    // 挂起备注
	HangUpName     string  `db:"hang_up_name" json:"hang_up_name" cbor:"hang_up_name"`          //  挂起人名字
	RemarkID       int     `db:"remark_id" json:"remark_id" cbor:"remark_id"`                   // 挂起原因ID
	HangUpAt       int     `db:"hang_up_at" json:"hang_up_at" cbor:"hang_up_at"`                //  挂起时间
	ReceiveAt      int64   `db:"receive_at" json:"receive_at" cbor:"receive_at"`                //领取时间
	Balance        string  `db:"balance" json:"balance" cbor:"balance"`                         //提现前的余额
	Flag           string  `json:"flag" db:"flag"  cbor:"flag"`                                 //1 cpf 2 phone 3 email

}

type TblPayFactoryData struct {
	T int             `json:"t" cbor:"t"`
	D []TblPayFactory `json:"d" cbor:"d"`
	R float64         `json:"r" cbor:"r"`
	C string          `json:"c" cbor:"c"`
}

type TblMemberBalance struct {
	Prefix            string  `json:"prefix" db:"prefix" cbor:"prefix"`
	Uid               string  `json:"uid" db:"uid" cbor:"uid"`
	Brl               float64 `json:"brl" db:"brl" cbor:"brl"`                                               //全部余额
	BrlAmount         float64 `json:"brl_amount" db:"-" cbor:"brl_amount"`                                   //可提现余额
	UnlockAmount      float64 `json:"unlock_amount" db:"unlock_amount" cbor:"unlock_amount"`                 //解锁余额
	LockAmount        float64 `json:"lock_amount" db:"lock_amount" cbor:"lockAmount"`                        //提现中余额
	AgencyAmount      float64 `json:"agency_amount" db:"agency_amount" cbor:"agencyAmount"`                  //推广账户余额
	DepositAmount     float64 `json:"deposit_amount" db:"-" cbor:"deposit_amount"`                           //存款账户余额
	DepositBalance    float64 `json:"deposit_balance" db:"-" cbor:"deposit_balance"`                         //存款可提现余额
	DepositLockAmount float64 `json:"deposit_lock_amount" db:"deposit_lock_amount" cbor:"depositLockAmount"` //存款不可提现余额
	AgencyBalance     float64 `json:"agency_balance" db:"-" cbor:"agency_balance"`                           //推广账户可提现余额
	AgencyLockAmount  float64 `json:"agency_lock_amount" db:"agency_lock_amount" cbor:"agencyLockAmount"`    //推广账户不可提现余额
}

type TblMemberBankcard struct {
	Id         string `json:"id" db:"id" cbor:"id"`
	Uid        string `json:"uid" db:"uid" cbor:"uid"`
	Username   string `json:"username" db:"username" cbor:"username"`
	CreatedAt  int    `json:"created_at" db:"created_at" cbor:"created_at"`
	State      int    `json:"state" db:"state" cbor:"state"`
	PixId      string `json:"pix_id" db:"pix_id" cbor:"pix_id"`
	Flag       int    `json:"flag" db:"flag" cbor:"flag"`
	BankName   string `json:"bankname" db:"bankname" cbor:"bankname"`
	BankCode   string `json:"bankcode" db:"bankcode" cbor:"bankcode"`
	PixAccount string `json:"pix_account" db:"pix_account" cbor:"pix_account"`
	RealName   string `json:"real_name" db:"real_name" cbor:"real_name"`
}

type Email struct {
	URL      string `toml:"url"`
	Port     string `toml:"port"`
	Username string `toml:"username"`
	Password string `toml:"password"`
}

type AwsEmail struct {
	AccessKeyID     string `toml:"accessKeyID"`
	SecretAccessKey string `toml:"secretAccessKey"`
	Region          string `toml:"region"`
	From            string `toml:"from"`
}

type TgPay struct {
	AppKey string `toml:"appKey"`
}
