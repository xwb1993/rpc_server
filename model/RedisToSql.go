package model

import (
	myRedis "common/redis"
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	"strconv"
	"strings"
	"time"
)

/*
更新日志到SQL
*/
var InsertLogCount = 100

func UpdateLogToSql(redis *redis.Client, sqlCon *sqlx.DB) {
	if redis == nil || sqlCon == nil {
		return
	}

	for {
		for {
			valSql, popError := redis.RPop(context.Background(), myRedis.UpdateSqlField).Result()
			if popError != nil {
				break
			}
			_, err := sqlCon.Exec(valSql)
			if err != nil {
				//需要写日志
				fmt.Println(valSql)
			}
		}

		for loopIndex := 0; loopIndex < InsertLogCount; loopIndex++ {
			valSql, popError := redis.RPop(context.Background(), myRedis.InsertLogKey).Result()
			if popError != nil {
				break
			}
			_, err := sqlCon.Exec(valSql)
			if err != nil {
				//需要写日志
				fmt.Println(valSql)
			}
		}

		time.Sleep(100 * time.Millisecond) // 等待时间间隔后重试
	}
}

/*
更新用户数据到SQL
*/
var UpdateUserDateCount = 100
var AllMemTableName [11]string = [11]string{
	"tbl_member_adjust", "tbl_member_balance", "tbl_member_bankcard", "tbl_member_base", "tbl_member_deposit_info",
	"tbl_member_level_downgrade", "tbl_member_level_record", "tbl_member_platform", "tbl_member_proxy_accu", "tbl_member_return",
}

var g_ThirdGameType [6]string = [6]string{"pp", "pg", "tada", "jbb", "evo", "jili"}
var g_ThirdGameID [6]int32 = [6]int32{100, 101, 102, 103, 104, 405}
var g_StatisticsType [4]string = [4]string{"running", "gameTax", "gameWinLost", "gameRound"}

func UpdateProxyStatisticsData(ctx context.Context, redis *redis.Client, sqlCon *sqlx.DB, valUid string) {

	redisKey := fmt.Sprintf("UserId:%s", valUid)
	timeStamp := time.Now().Unix()
	var gameType int
	var updateKey string
	for gameType = 0; gameType < 6; gameType++ {
		updateKey = fmt.Sprintf("%s:%s", g_ThirdGameType[gameType], g_StatisticsType[0])
		running, _ := redis.HGet(ctx, redisKey, updateKey).Result()

		updateKey = fmt.Sprintf("%s:%s", g_ThirdGameType[gameType], g_StatisticsType[1])
		gameTax, _ := redis.HGet(ctx, redisKey, updateKey).Result()

		updateKey = fmt.Sprintf("%s:%s", g_ThirdGameType[gameType], g_StatisticsType[2])
		gameWinLost, _ := redis.HGet(ctx, redisKey, updateKey).Result()

		updateKey = fmt.Sprintf("%s:%s", g_ThirdGameType[gameType], g_StatisticsType[3])
		strGameRound, _ := redis.HGet(ctx, redisKey, updateKey).Result()
		var uid, nGameRound int64
		var flRunning, flGameWinLost, flGameTax float64
		uid, _ = strconv.ParseInt(valUid, 10, 64)
		nGameRound, _ = strconv.ParseInt(strGameRound, 10, 64)
		flRunning, _ = strconv.ParseFloat(running, 64)
		flGameWinLost, _ = strconv.ParseFloat(gameWinLost, 64)
		flGameTax, _ = strconv.ParseFloat(gameTax, 64)
		if running != "" || gameTax != "" || gameWinLost != "" {
			_, errExec := sqlCon.Exec("CALL tbl_report_user_daily_update(?,?,?,?,?,?,?)", uid, g_ThirdGameID[gameType],
				flRunning, flGameWinLost, flGameTax, nGameRound, timeStamp)
			if errExec == nil {
				//以增量累加的方式更新到数据库 设置成功后需要清除
				updateKey = fmt.Sprintf("%s:%s", g_ThirdGameType[gameType], g_ThirdGameType[0])
				redis.HSet(ctx, redisKey, updateKey, "")
				updateKey = fmt.Sprintf("%s:%s", g_ThirdGameType[gameType], g_ThirdGameType[1])
				redis.HSet(ctx, redisKey, updateKey, "")
				updateKey = fmt.Sprintf("%s:%s", g_ThirdGameType[gameType], g_ThirdGameType[2])
				redis.HSet(ctx, redisKey, updateKey, "")
				updateKey = fmt.Sprintf("%s:%s", g_ThirdGameType[gameType], g_ThirdGameType[3])
				redis.HSet(ctx, redisKey, updateKey, "")
			} else {
				fmt.Printf("%s", errExec.Error())
			}
		}
	}

	deposit, errDeposit := redis.HGet(ctx, redisKey, "depoist").Result()
	if errDeposit == nil && deposit != "" {
		_, errExec := sqlCon.Exec("CALL tbl_report_user_business_deposit_update(?,?,?)", valUid, deposit, timeStamp)
		if errExec == nil {
			redis.HSet(ctx, redisKey, "deposit", "")
		} else {
			fmt.Printf("%s", errExec.Error())
		}
	}

	withdraw, errwithdraw := redis.HGet(ctx, redisKey, "withdraw").Result()
	if errwithdraw == nil && withdraw != "" {
		_, errExec := sqlCon.Exec("CALL tbl_report_user_business_withdraw_update(?,?,?)", valUid, withdraw, timeStamp)
		if errExec == nil {
			redis.HSet(ctx, redisKey, "deposit", "")
		} else {
			fmt.Printf("%s", errExec.Error())
		}
	}
}

func UpdateUserDataByUid(ctx context.Context, redis *redis.Client, sqlCon *sqlx.DB, valUid string) {
	redisKey := fmt.Sprintf("UserId:%s", valUid)
	for tableIndex := range AllMemTableName {
		tableName := AllMemTableName[tableIndex]
		updateField, err := redis.HGet(ctx, redisKey, tableName).Result()
		if err != nil {
			continue
		}

		tableFields := strings.Split(updateField, ":")
		if len(updateField) == 0 {
			continue
		}

		for arrIndex := 0; arrIndex < len(tableFields); arrIndex++ {
			updateKey := fmt.Sprintf("%s:%s", tableName, tableFields[arrIndex])
			updateValue, errGet := redis.HGet(ctx, redisKey, updateKey).Result()
			if errGet != nil {
				continue
			}

			//此处可优化 不需要每次都去查 做个新表旧表的判断就可以了
			var strQueryId string
			exceSqlQuery := fmt.Sprintf("SELECT uid FROM  %s where uid=%s", tableName, valUid)
			err := sqlCon.Get(&strQueryId, exceSqlQuery)
			if err == nil {
				exceSql := fmt.Sprintf("UPDATE %s set %s='%s' where uid='%s'", tableName, tableFields[arrIndex], updateValue, valUid)
				fmt.Println(exceSql)
				sqlCon.Exec(exceSql)
			} else {
				exceSql := fmt.Sprintf("insert into  %s(uid,%s) values(%s,%s)", tableName, tableFields[arrIndex], valUid, updateValue)
				sqlCon.Exec(exceSql)
			}
		}
		redis.HSet(ctx, redisKey, tableName, "")
		redis.HSet(ctx, redisKey, "lastUpdateTime", time.Now().Unix())
	}

}
func UpdateUserDataToSql(ctx context.Context, redis *redis.Client, sqlCon *sqlx.DB) {
	if redis == nil || sqlCon == nil {
		return
	}

	for {
		//先处理需要立即更新了用户数据
		for loopIndex := 0; loopIndex < UpdateUserDateCount; loopIndex++ {
			valUid, popError := redis.SPop(context.Background(), myRedis.ImmediateRoleSetKey).Result()
			if popError != nil {
				break
			}
			UpdateProxyStatisticsData(ctx, redis, sqlCon, valUid)
			UpdateUserDataByUid(ctx, redis, sqlCon, valUid)
		}

		members, err := redis.SMembers(context.Background(), "UpdateRoleSetKey").Result()
		if err != nil {
			break
		}

		// 输出集合中的所有成员
		for _, valUid := range members {
			redisKey := fmt.Sprintf("UserId:%s", valUid)

			//更新时间判断
			lastUpdateTime, _ := redis.HGet(ctx, redisKey, "lastUpdateTime").Result()
			nUpdateTime, _ := strconv.ParseInt(lastUpdateTime, 10, 64)
			fmt.Println(valUid)
			fmt.Println(time.Now().Unix())
			fmt.Println(nUpdateTime)
			if (time.Now().Unix() - nUpdateTime) <= int64(5*time.Minute) {
				continue
			}

			dataFieldLock := fmt.Sprintf("%s", valUid)
			myRedis.Lock(GetRedisInstance(), dataFieldLock)
			UpdateProxyStatisticsData(ctx, redis, sqlCon, valUid)
			UpdateUserDataByUid(ctx, redis, sqlCon, valUid)
			redis.SPop(context.Background(), valUid)
			myRedis.Unlock(GetRedisInstance(), dataFieldLock)
		}

		time.Sleep(50 * time.Millisecond) // 等待时间间隔后重试
	}
}
