package model

import (
	"database/sql"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/go-redis/redis/v8"
	_ "github.com/go-sql-driver/mysql"
	rysrv "github.com/ryrpc/server"
	"github.com/valyala/fasthttp"
	"rpc_server/contrib/helper"
	"strings"
)

type SelectParam struct {
	TableName      string      `json:"table_name" cron:"table_name"`
	Record         interface{} `json:"record" cron:"record"`
	Data           interface{} `json:"data" cron:"data"`
	QueryCondition g.Ex        `json:"query_condition" cron:"query_condition"`
	//QueryCondition exp.Expression          `json:"query_condition"`
	OperateRedis bool   `json:"operate_redis" cron:"operate_redis"`
	RedisKey     string `json:"redis_key" cron:"redis_key"`
}

// 查询
func SelectData(fctx *fasthttp.RequestCtx) {
	var (
		params SelectParam
	)

	err := rysrv.Unmarshal(fctx, &params)
	if err != nil {
		rysrv.SetError(fctx, err)
		return
	}
	fmt.Println(params)
	// 如果操作 Redis

	if params.OperateRedis {
		jsonData, err := meta.MerchantRedis.Get(ctx, params.RedisKey).Bytes()
		fmt.Println("rediserr", err)
		fmt.Println("redisnilerr", redis.Nil)
		if err == redis.Nil {
			fmt.Printf("RecordType1===: %T\n", params.Record)
			query, _, _ := dialect.From(params.TableName).Select(params.Record).Where(params.QueryCondition).ToSQL()
			query = strings.ReplaceAll(query, "\"", "`")
			fmt.Println("resp", query)
			// 打印确认数据类型
			fmt.Printf("params.DataType1===: %T\n", params.Data)
			//data := params.Data.(*TblMemberBalance)
			data := Switch(params.TableName)
			fmt.Printf("dataType1===: %T\n", data)
			err = meta.MerchantDB.Get(data, query)
			if err != nil {
				fmt.Println("err3", err, helper.DBErr)
			}
			params.Data = data

			fmt.Println("params.Data===", params.Data)
			// 打印确认数据类型
			fmt.Printf("Type2===: %T\n", params.Data)
			if err != nil && err != sql.ErrNoRows {
				fmt.Println("err1", err, helper.DBErr)
			}
			if err == sql.ErrNoRows {
				fmt.Println("err2", err, helper.DBErr)
			}

			redisData, err := helper.JsonMarshal(params.Data)
			if err != nil {
				fmt.Println("err3", err, helper.RedisErr)
			}
			if err := meta.MerchantRedis.Set(ctx, params.RedisKey, redisData, 0).Err(); err != nil {
				fmt.Println("err4", err, helper.RedisErr)
			}

		} else if err != nil {
			fmt.Println(helper.RedisErr) // 处理其他可能的错误
		} else {
			data := Switch(params.TableName)
			if err := helper.JsonUnmarshal(jsonData, data); err != nil {
				fmt.Println("err5", err, helper.RedisErr)
			}
			params.Data = data // 更新原来的 Data 值
			fmt.Println("params.Data==", params.Data)
			// 打印确认数据类型
			fmt.Printf("Type2: %T\n", params.Data)
		}
	} else {
		query, _, _ := dialect.From(params.TableName).Select(params.Record).Where(params.QueryCondition).ToSQL()
		query = strings.ReplaceAll(query, "\"", "`")
		fmt.Println("resp", query)
		data := Switch(params.TableName)
		err = meta.MerchantDB.Get(data, query)
		params.Data = data
		if err != nil && err != sql.ErrNoRows {
			fmt.Println("err1", err, helper.DBErr)
		}
		if err == sql.ErrNoRows {
			fmt.Println("err2", err, helper.DBErr)
		}
	}

	rysrv.SetResult(fctx, params.Data)
}

// 修改
func UpdateData(fctx *fasthttp.RequestCtx) {
	var (
		params SelectParam
	)
	err := rysrv.Unmarshal(fctx, &params)
	if err != nil {
		rysrv.SetError(fctx, err)
	}

	fmt.Println(params)
	// 如果操作 Redis
	if params.OperateRedis {
		_, err := meta.MerchantRedis.Get(ctx, params.RedisKey).Bytes()
		fmt.Println("rediserr", err)
		fmt.Println("redisnilerr", redis.Nil)
		if err == redis.Nil {
			fmt.Println("params.Record", params.Record)
			fmt.Printf("RecordType1===: %T\n", params.Record)
			keyStr, keyOK := params.Record.(map[interface{}]interface{})
			if !keyOK {
				fmt.Printf("keyStr===: %T\n", keyStr)
			}
			recordMap := convertMap(keyStr)
			recordData, err := json.Marshal(recordMap)
			if err != nil {
				fmt.Println("err1", err, helper.FormatErr)
			}
			//var memBalance TblMemberBalance
			data := Switch(params.TableName)
			err = json.Unmarshal(recordData, data)
			if err != nil { // or some other validation
				fmt.Println("err2", err, helper.FormatErr)
			}
			fmt.Println("memBalance", data)
			fmt.Printf("memBalanceType1===: %T\n", data)
			//query, _, _ := dialect.From(params.TableName).Select(params.Record).Where(params.QueryCondition).ToSQL()
			query, _, err := dialect.Update(params.TableName).Set(data).Where(params.QueryCondition).ToSQL()
			if err != nil {
				fmt.Println("err1", err, helper.DBErr)
			}
			fmt.Println("err前", err)
			fmt.Println("query前", query)
			query = strings.ReplaceAll(query, "\"", "`")
			fmt.Println("query后", query)
			_, err = meta.MerchantDB.Exec(query)
			if err != nil {
				fmt.Println("err1", err, helper.DBErr)
			}
		} else if err != nil {
			fmt.Println(helper.RedisErr) // 处理其他可能的错误
		} else {
			data := Switch(params.TableName)
			keyStr, keyOK := params.Record.(map[interface{}]interface{})
			if !keyOK {
				fmt.Printf("keyStr===: %T\n", keyStr)
			}
			recordMap := convertMap(keyStr)
			recordData, err := json.Marshal(recordMap)
			if err != nil {
				fmt.Println("err1", err, helper.FormatErr)
			}
			err = json.Unmarshal(recordData, data)
			if err != nil { // or some other validation
				fmt.Println("err2", err, helper.FormatErr)
			}
			redisData, err := helper.JsonMarshal(data)
			//redisData, err := helper.JsonMarshal(params.Record)
			if err != nil {
				fmt.Println("err3", err, helper.RedisErr)
			}
			meta.MerchantRedis.Del(ctx, params.RedisKey)
			if err := meta.MerchantRedis.Set(ctx, params.RedisKey, redisData, 0).Err(); err != nil {
				fmt.Println("err4", err, helper.RedisErr)
			}

			query, _, err := dialect.Update(params.TableName).Set(data).Where(params.QueryCondition).ToSQL()
			if err != nil {
				fmt.Println("err1", err, helper.DBErr)
			}
			fmt.Println("err前", err)
			fmt.Println("query前", query)
			query = strings.ReplaceAll(query, "\"", "`")
			fmt.Println("query后", query)
			if err = meta.MerchantRedis.LPush(ctx, "sqlqueue", query).Err(); err != nil {
				fmt.Println("err5", err, helper.RedisErr)
			}
		}
	} else {
		keyStr, keyOK := params.Record.(map[interface{}]interface{})
		if !keyOK {
			fmt.Printf("keyStr===: %T\n", keyStr)
		}
		recordMap := convertMap(keyStr)
		recordData, err := json.Marshal(recordMap)
		if err != nil {
			fmt.Println("err1", err, helper.FormatErr)
		}
		data := Switch(params.TableName)
		//var memBalance TblMemberBalance
		err = json.Unmarshal(recordData, data)
		if err != nil { // or some other validation
			fmt.Println("err2", err, helper.FormatErr)
		}
		query, _, err := dialect.Update(params.TableName).Set(data).Where(params.QueryCondition).ToSQL()
		if err != nil {
			fmt.Println("err1", err, helper.DBErr)
		}
		query = strings.ReplaceAll(query, "\"", "`")
		_, err = meta.MerchantDB.Exec(query)
		if err != nil {
			fmt.Println("err1", err, helper.DBErr)
		}
	}

	rysrv.SetResult(fctx, true)
}

// 新增
func InsertData(fctx *fasthttp.RequestCtx) {
	var (
		params SelectParam
	)

	err := rysrv.Unmarshal(fctx, &params)
	if err != nil {
		rysrv.SetError(fctx, err)
	}
	fmt.Println(params)
	fmt.Println("params.Record", params.Record)
	fmt.Printf("RecordType1===: %T\n", params.Record)
	query, _, _ := dialect.Insert(params.TableName).Rows(params.Record).ToSQL()
	fmt.Println(query)
	query = strings.ReplaceAll(query, "\"", "`")
	fmt.Println(query)
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		body := fmt.Errorf("%s,[%s]", err.Error(), query)
		fmt.Println("err1", body)
	}

	rysrv.SetResult(fctx, true)
}

// 删除
func DeleteData(fctx *fasthttp.RequestCtx) {
	var (
		params SelectParam
	)
	err := rysrv.Unmarshal(fctx, &params)
	if err != nil {
		rysrv.SetError(fctx, err)
	}

	fmt.Println(params)

	meta.MerchantRedis.Del(ctx, params.RedisKey)
	fmt.Println("memBalance", params.QueryCondition)
	fmt.Printf("memBalanceType1===: %T\n", params.QueryCondition)
	query, _, err := dialect.Delete(params.TableName).Where(params.QueryCondition).ToSQL()
	if err != nil {
		fmt.Println("err1", err, helper.DBErr)
	}
	query = strings.ReplaceAll(query, "\"", "`")
	fmt.Println("query后", query)
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		fmt.Println("err1", err, helper.DBErr)
	}

	rysrv.SetResult(fctx, true)
}

func Switch(tableName string) interface{} {
	var data interface{}
	switch tableName {
	case "tbl_admin_group":
		data = &TblAdminGroup{}
	case "tbl_admin_group_tree":
		data = &TblAdminGroupTree{}
	case "tbl_admin_priv":
		data = &TblAdminPriv{}
	case "tbl_admins":
		data = &TblAdmins{}
	case "tbl_app_upgrade":
		data = &TblAppUpgrade{}
	case "tbl_balance_transaction":
		data = &TblBalanceTransaction{}
	case "tbl_banktype":
		data = &TblBankType{}
	case "tbl_banner":
		data = &TblBanner{}
	case "tbl_bonus_config":
		data = &TblBonusConfig{}
	case "tbl_business_info":
		data = &TblBusinessInfo{}
	case "tbl_deposit":
		data = &tblDeposit{}
	case "tbl_flow_config":
		data = &TblFlowConfig{}
	case "tbl_game_config":
		data = &TblGameConfig{}
	case "tbl_game_lists":
		data = &TblGameLists{}
	case "tbl_game_record":
		data = &TblGameRecord{}
	case "tbl_game_tag_idx":
		data = &TblGameTagIdx{}
	case "tbl_game_tag_name":
		data = &TblGameTagName{}
	case "tbl_member_adjust":
		data = &TblMemberAdjust{}
	case "tbl_member_balance":
		data = &TblMemberBalance{}
	case "tbl_member_bankcard":
		data = &TblMemberBankcard{}
	//case "tbl_member_base":
	//	data = &TblMemberBase{}
	case "tbl_member_deposit_info":
		data = &TblMemberDepositInfo{}
	case "tbl_member_level_downgrade":
		data = &TblMemberLevelDowngrade{}
	case "tbl_member_level_record":
		data = &TblMemberLevelRecord{}
	case "tbl_member_platform":
		data = &TblMemberPlatform{}
	case "tbl_member_test":
		data = &TblMemberTest{}
	case "tbl_members_tree":
		data = &TblMembersTree{}
	case "tbl_messages":
		data = &TblMessage{}
	case "tbl_notices":
		data = &TblNotices{}
	case "tbl_operator_domain":
		data = &TblOperatorDomain{}
	case "tbl_operator_info":
		data = &TblOperatorInfo{}
	case "tbl_pay_factory":
		data = &TblPayFactory{}
	case "tbl_pdd_turntable_history":
		data = &TblPddTurntableHistory{}
	case "tbl_pdd_turntable_info":
		data = &TblPddTurntableInfo{}
	case "tbl_pdd_turntable_review":
		data = &TblPddTurntableReview{}
	case "tbl_platforms":
		data = &TblPlatforms{}
	case "tbl_promo_deposit":
		data = &TblPromoDeposit{}
	case "tbl_promo_inspection":
		data = &TblPromoInspection{}
	case "tbl_promo_invite_record":
		data = &TblPromoInviteRecord{}
	case "tbl_promo_sign_config":
		data = &TblPromoSignConfig{}
	case "tbl_promo_sign_record":
		data = &TblPromoSignRecord{}
	case "tbl_promo_sign_reward_record":
		data = &TblPromoSignRewardRecord{}
	case "tbl_promo_treasure_config":
		data = &TblPromoTreasureConfig{}
	case "tbl_promo_treasure_record":
		data = &TblPromoTreasureRecord{}
	case "tbl_promo_weekbet_config":
		data = &TblPromoWeekBetConfig{}
	case "tbl_promo_weekbet_record":
		data = &TblPromoWeekbetRecord{}
	case "tbl_rebate_record":
		data = &TblRebateRecord{}
	case "tbl_report_agency":
		data = &TblReportAgency{}
	case "tbl_report_daily":
		data = &TblReportDaily{}
	case "tbl_report_game":
		data = &TblReportGame{}
	case "tbl_report_game_daily":
		data = &TblReportGameDaily{}
	case "tbl_report_platform":
		data = &TblReportPlatform{}
	case "tbl_report_proxy_daily":
		data = &TblReportProxyDaily{}
	case "tbl_report_proxy_daily_1":
		data = &TblReportProxyDaily1{}
	case "tbl_report_user":
		data = &TblReportUser{}
	case "tbl_report_user_daily":
		data = &TblReportUserDaily{}
	case "tbl_transaction_types":
		data = &TblTransactionTypes{}
	case "tbl_whitelist":
		data = &TblWhitelist{}
	case "tbl_withdraw":
		data = &TblWithdraw{}
	}
	return data
}

func convertMap(in map[interface{}]interface{}) map[string]interface{} {
	out := make(map[string]interface{})
	for key, value := range in {
		strKey, ok := key.(string)
		if ok {
			out[strKey] = value
		}
	}
	return out
}
