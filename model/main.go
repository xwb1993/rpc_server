package model

import (
	"context"
	g "github.com/doug-martin/goqu/v9"
	"github.com/go-redis/redis/v8"
	"github.com/ip2location/ip2location-go/v9"
	"github.com/jmoiron/sqlx"
	"github.com/meilisearch/meilisearch-go"
	"github.com/valyala/fasthttp"
	"lukechampine.com/frand"
	"rpc_server/contrib/conn"
	"time"
)

type MetaTable struct {
	MerchantDB    *sqlx.DB
	MerchantBean  *conn.Connection
	MerchantRedis *redis.Client
	IpDB          *ip2location.DB
	Program       string
	SecretKey     string
	Callback      string
	WebUrl        string
	Contate       string
	WalletMode    string
	Email         Email
	AwsEmail      AwsEmail
	Rand          *frand.RNG
	Meili         *meilisearch.Client
	TgPay         TgPay
}

var (
	loc     *time.Location
	meta    *MetaTable
	ctx     = context.Background()
	dialect = g.Dialect("mysql")
)

func GetDBInstance() *sqlx.DB {
	return meta.MerchantDB
}

func GetRedisInstance() *redis.Client {
	return meta.MerchantRedis
}

func redisx(fctx *fasthttp.RequestCtx) *redis.Client {

	temp := fctx.UserValue("redis")

	return temp.(*redis.Client)
}

func db(fctx *fasthttp.RequestCtx) *sqlx.DB {

	temp := fctx.UserValue("db")

	return temp.(*sqlx.DB)
}

func meili(fctx *fasthttp.RequestCtx) *meilisearch.Client {

	temp := fctx.UserValue("meili")

	return temp.(*meilisearch.Client)
}
func Constructor(mt *MetaTable) {
	meta = mt
}

func Close() {

	meta.MerchantBean.Conn.Release()
	meta.MerchantDB.Close()
	meta.MerchantRedis.Close()
}
