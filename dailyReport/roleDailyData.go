package dailyReport

import (
	"context"
	"fmt"
	"github.com/robfig/cron/v3"
	rysrv "github.com/ryrpc/server"
	"github.com/valyala/fasthttp"
	"rpc_server/model"
	"strconv"
	"strings"
	"sync"
	"time"
)

type UserGameBaseDataItem struct {
	uGameData      [game_max]gameRoundDataItem
	lastUpdateTime int64
}

func (valItem *UserGameBaseDataItem) InitUserGameBaseDataItem() {
	for i := 0; i < game_max; i++ {
		valItem.uGameData[i].InitGameRoundDataItem()
	}
}

func (valItem *UserGameBaseDataItem) IsValidData() bool {
	for i := 0; i < game_max; i++ {
		if valItem.uGameData[i].IsValidData() {
			return true
		}
	}

	return false
}

var mapUserGameData = make(map[int32]*UserGameBaseDataItem)

// 互斥锁
var HcMutex sync.Mutex

func RpcUserDailyData(fctx *fasthttp.RequestCtx) {

	var params gameRoundDataItem
	err := rysrv.Unmarshal(fctx, &params)

	if err != nil {
		rysrv.SetError(fctx, err)
		return
	}

	//AddGameRoundData(params)
	rysrv.SetResult(fctx, true)
}

/*
从Redis数据库中加载数据 用于程序刚启动时
*/
func loadGameRoundDataFromRedis() {

	iter := model.GetRedisInstance().Scan(context.Background(), 0, "userGameData_*", 0).Iterator()
	for iter.Next(context.Background()) {
		redisKey := iter.Val()
		var uGameData gameRoundDataItem
		for i := 0; i < game_max; i++ {
			GameTypeKey := fmt.Sprintf("%d", i)
			GameDatafield, errGet := model.GetRedisInstance().HGet(context.Background(), redisKey, GameTypeKey).Result()
			if errGet == nil {
				(&uGameData).InitGameRoundDataItem()

				ssData := strings.Split(GameDatafield, "-")
				uid, _ := strconv.Atoi(ssData[0])
				gameType, _ := strconv.Atoi(ssData[1])
				gameRunning, _ := strconv.ParseFloat(ssData[2], 64)
				gameWinLost, _ := strconv.ParseFloat(ssData[3], 64)
				gameTax, _ := strconv.ParseFloat(ssData[4], 64)

				uGameData.uid = int32(uid)
				uGameData.gameType = int32(gameType)
				uGameData.gameRunning = gameRunning
				uGameData.gameWinLost = gameWinLost
				uGameData.gameTax = gameTax

				//AddGameRoundData(uGameData)
			}
		}

	}
}

func ProcessBusinessData(dataItem userBusinessDataItem) {
	execsql := fmt.Sprintf("CALL tbl_report_user_bussness_update(%d,%d,%.2f,%d)", dataItem.uid, dataItem.dataType, dataItem.dataValue, time.Now().Unix())
	queryRow := model.GetDBInstance().QueryRowContext(context.Background(), execsql)
	if queryRow.Err() != nil {
		fmt.Println(queryRow.Err().Error())
	}
}

func StartReportService() {

	//add test

	//AddGameRoundData(gameRoundDataItem{7, 0, 2.3, 0.8, 0.01})
	//AddGameRoundData(gameRoundDataItem{7, 1, 2.3, 0.8, 0.01})
	//AddGameRoundData(gameRoundDataItem{7, 2, 2.3, 0.8, 0.01})
	//return
	c := cron.New(cron.WithSeconds())
	c.Start()
}

// /////删除过期且无效的数据
func DeleteExpiredKey() {
	HcMutex.Lock()
	timestamp := time.Now().Unix()
	for key, val := range mapUserGameData {
		if !val.IsValidData() &&
			timestamp >= (val.lastUpdateTime+60) {
			delete(mapUserGameData, key)
		}
	}

	HcMutex.Unlock()
}

// ////同步数据到MySQL
func SyncUserGameDailyDataToDB() {

	HcMutex.Lock()
	ctx := context.Background()
	for key, val := range mapUserGameData {
		for i := 0; i < game_max; i++ {
			if !val.uGameData[i].IsValidData() {
				continue
			}

			execsql := fmt.Sprintf("CALL tbl_report_user_daily_update(%d,%d,%.2f,%.2f,%.2f,%d)",
				val.uGameData[i].uid, val.uGameData[i].gameType, val.uGameData[i].gameRunning, val.uGameData[i].gameWinLost, val.uGameData[i].gameTax, val.lastUpdateTime)
			queryRow := model.GetDBInstance().QueryRowContext(ctx, execsql)
			if queryRow.Err() == nil {
				redisKey := fmt.Sprintf("userGameData_%d", key)
				GameTypeKey := fmt.Sprintf("%d", val.uGameData[i].gameType)
				redisVal := fmt.Sprintf("%d-%d-0.0-0.0-0.0",
					val.uGameData[i].uid,
					val.uGameData[i].gameType)
				model.GetRedisInstance().HSet(context.Background(), redisKey, GameTypeKey, redisVal)
				(&val.uGameData[i]).InitGameRoundDataItem()
			} else {
				fmt.Println(queryRow.Err().Error())
			}
		}

		val.InitUserGameBaseDataItem()
	}

	HcMutex.Unlock()
}
