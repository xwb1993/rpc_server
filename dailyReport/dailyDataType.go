package dailyReport

// 用户业务数据数据 用于统计 充值 提现 奖励 等等
// //用户业务 数据类型定义
const (
	busi_Deposit = iota
	busi_Withdraw
	busi_max
)

type userBusinessDataItem struct {
	uid       int32 //玩家ID
	dataType  int
	dataValue float64
}

// //third_game defind
const (
	game_pp = iota
	game_pg
	game_evo
	game_max
)

// 一局游戏的数据 用于统计
type gameRoundDataItem struct {
	uid         int32 //玩家ID
	gameType    int32 //三方游戏类型：PP PG
	gameRunning float64
	gameWinLost float64
	gameTax     float64
}

func (valItem *gameRoundDataItem) InitGameRoundDataItem() {
	valItem.uid = 0
	valItem.gameType = 0.0
	valItem.gameRunning = 0.0
	valItem.gameWinLost = 0
	valItem.gameTax = 0.0
}

func (valItem *gameRoundDataItem) IsValidData() bool {
	if valItem.uid == 0 &&
		valItem.gameType == 0 &&
		valItem.gameRunning == 0.0 &&
		valItem.gameWinLost == 0.0 &&
		valItem.gameTax == 0.0 {
		return false
	}

	return true
}
