package main

import (
	"common/config"
	myredis "common/redis"
	"common/userHelp"
	"context"
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/meilisearch/meilisearch-go"
	rysrv "github.com/ryrpc/server"
	"github.com/valyala/fasthttp"
	_ "go.uber.org/automaxprocs"
	"log"
	"os"
	"rpc_server/contrib/apollo"
	"rpc_server/contrib/conn"
	"rpc_server/contrib/session"
	"rpc_server/dailyReport"
	"rpc_server/model"
	"rpc_server/pkg"
	"strings"
)

func main() {

	argc := len(os.Args)
	if argc != 3 {
		fmt.Printf("%s <etcds> <cfgPath>\r\n", os.Args[0])
		return
	}

	cfg := pkg.Conf{}
	endpoints := strings.Split(os.Args[1], ",")
	apollo.New(endpoints, pkg.ETCDName, pkg.ETCDPass)
	//err := apollo.ParseTomlStruct(os.Args[2], &cfg)
	if _, err := toml.DecodeFile(os.Args[2], &cfg); err != nil {
		fmt.Printf("ParseTomlStruct error: %s", err.Error())
		return
	}
	//apollo.New(endpoints, ETCDName, ETCDPass)
	//apollo.ParseTomlStruct(os.Args[2], &cfg)
	apollo.Close()

	mt := new(model.MetaTable)

	mt.MerchantDB = conn.InitDB(cfg.Db.Admin, cfg.Db.MaxIdleConn, cfg.Db.MaxOpenConn)
	mt.MerchantRedis = conn.InitRedis(cfg.Redis.Addr[0], cfg.Redis.Password, 0)
	session.New(mt.MerchantRedis, cfg.Prefix)
	model.Constructor(mt)
	//mt.Program = filepath.Base(os.Args[0])
	defer func() {
		model.Close()
		mt = nil
	}()
	meili := meilisearch.NewClient(meilisearch.ClientConfig{
		Host:   cfg.Meilisearch.Host,
		APIKey: cfg.Meilisearch.Key,
	})

	/*
		defer func() {
			model.Close()
			mt = nil
		}()
	*/
	config.InitCfg(mt.MerchantDB, mt.MerchantRedis)

	myredis.Init(context.Background(), mt.MerchantRedis, mt.MerchantDB, userHelp.LoadUserToRedis)
	dailyReport.StartReportService()
	repo := rysrv.NewRepository()

	repo.Use("meili", meili)
	repo.Register("/message/init", model.MessageInit)
	repo.Register("/message/list", model.MessageList)
	repo.Register("/message/bulk", model.MessageBulk)
	repo.Register("/message/delete/id", model.MessageDeleteById)
	repo.Register("/message/delete/filter", model.MessageDeleteByFilter)
	repo.Register("/message/update", model.MessageUpdate)
	repo.Register("/message/total/unread", model.MessageUnreadTotal)
	repo.Register("/rpc/test", model.MemberTest)
	repo.Register("/member/query", model.SelectData)
	repo.Register("/member/update", model.UpdateData)
	repo.Register("/member/insert", model.InsertData)
	repo.Register("/member/delete", model.DeleteData)

	myredis.AddProxyBusiDataByFloat64("9", "deposit", 100)
	myredis.AddProxyBusiDataByFloat64("9", "withdraw", 200)
	myredis.AddProxyBusiDataByFloat64("9", "pg:running", 1000)
	myredis.AddProxyBusiDataByFloat64("9", "pg:gameWinLost", 2000)
	myredis.AddProxyBusiDataByFloat64("9", "pg:gameTax", 3000)

	myredis.AddProxyBusiDataByFloat64("9", "pp:running", 1000)
	myredis.AddProxyBusiDataByFloat64("9", "pp:gameWinLost", 2000)
	myredis.AddProxyBusiDataByFloat64("9", "pp:gameTax", 3000)

	////add register daily
	repo.Register("/dailyReport/AddUserDailyData", dailyReport.RpcUserDailyData)
	go model.UpdateUserDataToSql(context.Background(), mt.MerchantRedis, mt.MerchantDB)
	go model.UpdateLogToSql(mt.MerchantRedis, mt.MerchantDB)

	/*
		uid := "9"
		deposit := "12.36"
		timestamp := time.Now().Unix()
		_, errExec := mt.MerchantDB.Exec("CALL tbl_report_user_business_deposit_update(?,?,?)", uid, deposit, timestamp)
		if errExec != nil {
			fmt.Printf("%s", errExec.Error())
		}
	*/
	err := fasthttp.ListenAndServe(cfg.Port.RPC, repo.RequestHandler())
	if err != nil {
		log.Fatalf("Error in ListenAndServe: %s", err.Error())
	}
}
