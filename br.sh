#! /bin/bash

git checkout main
git pull origin main
git submodule init
git submodule update

PROJECT="rpc_server"
GitReversion=`git rev-parse HEAD`
BuildTime=`date +'%Y.%m.%d.%H%M%S'`
BuildGoVersion=`go version`

CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "-X main.gitReversion=${GitReversion}  -X 'main.buildTime=${BuildTime}' -X 'main.buildGoVersion=${BuildGoVersion}'" -o $PROJECT
upx $PROJECT
# br
scp -i /opt/data/id_rsa_nginx -P 2223  $PROJECT brazil@23.234.61.35:/home/brazil/workspace/br/${PROJECT}/${PROJECT}_br
ssh -i /opt/data/id_rsa_nginx -p 2223 brazil@23.234.61.35 "sh /home/brazil/workspace/br/${PROJECT}/br.sh"

